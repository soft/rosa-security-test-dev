#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import psutil
import os
import grp
import pathlib
import select

from kplpack.utils import colored as I
from kplpack.utils import Walker
from kplpack.utils import get_file_mode


class WriteNotRead(Test):
  name = 'Утилита: проверка директории на объекты доступные к записи, но недоступные к чтению'
  exclusive = False

  # + /bin
  # + /dev
  #
  # settings
  target_dir = '/lost+found'
  exclude_groups = ['wheel']
  exclude_objects = ('/dev/urandom', '/dev/random', '/dev/zero')

  def available(self, file:pathlib.Path, mode='r' or 'w' or 'e', timeout:float=0.0):
    if mode == 'r':
      readable, _, _ = select.select([file], [], [], timeout)
      if file in readable:
        return True
    elif mode == 'w':
      _, writable, _ = select.select([], [file], [], timeout)
      if file in writable:
        return True
    elif mode == 'e':
      _, _, executable = select.select([], [], [file], timeout)
      if file in executable:
        return True

    return False

  def readable(self, file:pathlib.Path):
    try:
      with file.open( 'rb' ) as f:
        if self.available(f, 'r', 5):
          os.read(f.fileno(), 1)
          return True
        return None
    except (PermissionError, FileNotFoundError) as e:
      return False

  def writable(self, file:pathlib.Path, comment=''):
    print(2)
    try:
      with file.open( 'wb' ) as f:
        if self.available(f, 'w', 5):
          os.write(f.fileno(), b'')
          return True
        return None
    except (PermissionError, FileNotFoundError) as e:
      return False

  def cansave(self, file:pathlib.Path):
    print(3)
    old_content = None
    try:
      with file.open('rb') as backup_file:
        old_content = backup_file.read()
    except:
      return False

    try:
      with file.open('ab') as file_to_write:
        file_to_write.write('🎈'.encode())

      with file.open('rb') as file_to_rw:
        data = file_to_rw.read()
        if data.find('🎈'.encode()) >= 0:
          return True

    except OSError as e:
      return False

    finally:
      with file.open('wb') as restore_file:
        restore_file.write(old_content)


  def go(self, **kwargs):
    self.ui = kwargs.get('ui')

    current_user_login = os.getlogin()
    current_user_groups = [grp.getgrgid(g).gr_name for g in os.getgroups()]

    # exclude from checking
    for g in current_user_groups:
      if g in self.exclude_groups:
        current_user_groups.remove(g)

    errors = 0

    for i,entry in enumerate(Walker([self.target_dir]), start=1):

      if entry.as_posix().startswith(self.exclude_objects):
        log.warn(f'Объект {entry} исключен из проверки.')
        continue

      # need to work on the architecture. test should not have access to the gui
      if self.ui and not i % 100:
        estimate = self.progress_max - self.progress
        if estimate > 100:
          self.progress += 100
          self.ui.action( 'progress_bar|inc', 100 )
        else:
          self.progress += estimate
          self.ui.action( 'progress_bar|inc', estimate )

      try:
        if entry.exists() and not entry.is_symlink():
          mode = get_file_mode(entry)
          owner = entry.owner()
          group = entry.group()
          print(f'{i}: [{mode} {owner}:{group}] {I(entry, 111)}')

          if ( owner == current_user_login or mode[-1] in '2,3,6,7' or (group in current_user_groups and mode[-2] in '2,3,6,7')):

            readable = self.readable(entry)
            writable = self.writable(entry)

            if readable is None:
              print(f'Таймаут по чтению {entry}')
            if writable is None:
              print(f'Таймаут по записи {entry}')

            if writable and not readable:
              log.warn(f'Объект {entry} может быть использован для записи, но не для чтения.')
              continue
            elif not writable and not readable:
              print(f'Объект {entry} недоступен.')
              continue

            if self.cansave(entry):
              errors += 1
              log.error(f'Объект {entry} может хранить информацию!')

      except (OSError, KeyError, PermissionError):
        print(f'Не удалось получить информацию о {I(entry, 111)}, пропускаем...')

    if errors:
      log.error(f'Кол-во обнаруженных нарушений доступа: {I(str(errors), 9)}.')
    return errors


  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    path, files_count = next(Walker([self.target_dir]).count())
    return files_count