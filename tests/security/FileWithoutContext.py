#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

from psutil import disk_usage
from psutil import virtual_memory
from kplpack.utils import getcon
from kplpack.utils import User
from kplpack.utils import QuotedPath as Path

class FileWithoutContext(Test):

  name = 'Проверка ограничения возможности пользователя по созданию файлов без контекста безопасности'
  exclusive = False

  u = User.current(True)

  path_h = Path(Path.home() / '777.txt')
  path_t = Path('/tmp/777.txt')

  free_bytes = 1024 * 1024 * 1 # 1Mb

  def go(self, **kwargs):

    # if userConGet().u != 'user_u':
    #   print( f'Тест запущен с неправильным контекстом "{userConGet().u}", должен быть "user_u"! ', file=stderr )

    if disk_usage('/').free > self.free_bytes:

      with self.path_h.open('a+') as f:
        f.write('text')

      if getcon(self.path_h.as_posix()).str == 'file_not_found':
        raise error(f'Нарушение безопасности: файл {self.path_h.format(underline=True)} создан без контекста '
                     'безопасности!', 1)

    if virtual_memory().available > self.free_bytes \
      and disk_usage('/tmp').free > self.free_bytes:

      with self.path_t.open('a+') as f:
        f.write('text')

      if getcon(self.path_t.as_posix()).str == 'file_not_found':
        raise error(f'Нарушение безопасности: файл {self.path_t.format(underline=True)} создан без контекста безопасности!', 2)

    return 0

  def clean(self, **kwargs):
    if self.path_h.exists():
      self.path_h.unlink(True)
    if self.path_t.exists():
      self.path_t.unlink(True)

  def duration(self, **kwargs):
    pass