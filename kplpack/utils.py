#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

#  ┌──────────────────────────────────────────────────┐
#  │  kernelplv@gmail.com || m.mosolov@rosalinux.ru   │
#  │  © 2019-2020 www.rosalinux.ru                    │
#  │  utils.py - common utilities for testing         │
#  ╘══════════════════════════════════════════════════╛

import importlib
import sys
import time
import os
import crypt
import psutil
import shutil
import pwd
import grp
import hashlib
import logging
import re
import selinux
import tempfile
import pathlib
import stat
import cProfile
import pstats
import io

from pathlib import Path, PosixPath
from collections import namedtuple
from typing import NamedTuple, Tuple, Union, List

from subprocess import Popen, check_output, PIPE, STDOUT, run
from multiprocessing.synchronize import Lock as classLock
from threading import Thread, Lock
from enum import Enum

OK  = 0
BAD = 1


class StreamToLog(object):
  """ Fake file-like stream object that redirects writes to a logger instance.
      example: sys.stderr = StreamToLogger(logger, logging.DEBUG)
  """
  def __init__(self, logger, log_level=logging.INFO):

    self.logger    = logger
    self.log_level = log_level
    self.linebuf   = ''

    if logger.handlers:
      if hasattr( logger.handlers[0], 'baseFilename' ):
        with open( logger.handlers[0].baseFilename, 'r') as f:
          self.fileno  = f.fileno()

  def write(self, buf):

    for line in buf.rstrip().splitlines():

      if 'Gtk-WARNING' in line:
        continue
      else:
        self.logger.log(self.log_level, line.rstrip())

  def flush(self):
    pass

class StreamToMem(object):

  def __init__(self, mem_file):
    self.mem     = mem_file
    self.linebuf = ''

  def write(self, buf):
    for line in buf.splitlines():
        if 'Gtk-WARNING' in line:
          continue
        else:
          self.mem.write_bytes(line.encode())

  def flush(self):
    pass

class Damper:

  def __init__(self, stream = sys.stderr):
    if hasattr(stream, 'fileno') and hasattr(stream, 'flush'):
      self.stream = stream
      self.stream.flush()
      self.stream_file = self.stream.fileno()
      self.stream_backup = os.dup( self.stream_file )
    pass

  def __enter__(self):
    try:
      self.devnull = os.open( os.devnull, os.O_WRONLY )
      os.dup2( self.devnull, self.stream_file)
    finally:
      os.close( self.devnull )
    pass

  def __exit__(self, type, value, traceback):
    os.dup2(  self.stream_backup, self.stream_file )
    os.close( self.stream_backup )

    if value:
      raise Exception(value)

    pass

class Return(Enum):

  stdout  = 1
  codeout = 2
  errcode = 3
  process = 4
  ignore  = 5

class Command:

  def __init__(self):
    self.line          = ''
    self.back          = ''
    self.info          = ''
    self.error         = ''
    self.pause_before  = 0
    self.ignore_output = False
    self.ignore_errors = False
    self.only_errcode  = False
    self.thread        = False
    self.timeout       = 0
    pass

class Terminal:

  class TerminalException(Exception):
    message='Information. Terminal fake error.'
    pass

  def __init__(self, stream = sys.stdout, flush=True, back_after=False):
    """
      If backforce==True back-command is executed immediately after an error occurs
    """

    self.__commands    = []

    self.__threads     = []
    self.__lock        = Lock()

    self.__stream      = stream
    self.__flush       = flush
    self.__err_msg     = ''
    self.__back_after  = back_after

    self.log           = [ [0, 'Log start'] ]
    self.errors        = [ 0, 'No errors']


    pass

  def __str__(self):
    mxlinesize = 1

    for c in self.__commands:
      mxlinesize = max(mxlinesize, len(c.line), len(c.back) )

    out =  'Commands'.center( mxlinesize+7, '-' )
    out += 'Thread'.center(9, '-')
    out += 'Back'.center(6, '-')
    out += '\n'

    def makel(cmd, back=False):
      l = ''
      if back:
        l += ( ( '└ ' + cmd.back ) + ' ').ljust(mxlinesize+6,'.')
      else:
        l += (cmd.line + ' ').ljust(mxlinesize+6,'.')

      l += '|' + ( '+'   if cmd.thread else '-').center(6)  # thread col
      l += '|' + ( '+'   if back       else '-').center(7)  # back col
      l += '\n'
      return l

    for c in self.__commands:
      out += makel(c)
      if c.back:
        out += makel(c, True)

    return out

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):
    self.run()

    if not self.ok():
      print(self.errors[1], file=self.__stream)

    #self.errors = self.errors[0]

    pass

  def __soft_exit(self):
    for t in self.__threads:
      t.join()

    pass

  def __hard_exit(self):
    for t in self.__threads:
      t.join(timeout=0.1)

    pass

  def add(self, command='', thread=False, ignore_output=False, ignore_errors=False, only_errcode=False, pause_before=0, timeout=0):
    if command:
      #self.__commands.append( [command, thread, ignore_output, pause, ''] )
      cmd = Command()
      cmd.line          = command
      cmd.thread        = thread
      cmd.ignore_output = ignore_output
      cmd.ignore_errors = ignore_errors
      cmd.only_errcode  = only_errcode
      cmd.pause_before  = pause_before
      cmd.timeout       = timeout
      self.__commands.append( cmd )
      return self

  def ok(self, right_code=0):
    """
      Returns true if everything is OK (== right_code), otherwise fills the last error field
    """
    for l in self.log:
      if l[0] != right_code:
        if not l[1] and not self.__err_msg:
          self.errors = [ l[0], 'Error message not provided.' ]
        elif not self.__err_msg:
          self.errors = [ l[0], l[1] ]
        else:
          self.errors = [ l[0], self.__err_msg.format( code=l[0], msg=l[1] ) ]

        return False

    return True

  def info(self, msg):
    """
      Add description to last command.
      it will be sent to __stream before executing the command
    """

    if msg and self.__commands:
      self.__commands[-1].info = msg
    return self

  def error(self, msg='Error[{code}]: {msg}'):
    """
      Adds a general error message.
      If not specified, raw output is used.
      Example: msg='Message:{msg}, Code{code}'
    """

    if msg:
      self.__err_msg = msg

  def back(self, cmd):
    """
      Add a command that will lead to the opposite result.
      exapmle:
        line: setenforce 1
        back: setenforce 0
    """
    if cmd and self.__commands:
      self.__commands[-1].back = cmd

    return self

  def run(self, back=False):
    try:
      for cmd in self.__commands:

        if cmd.thread:
          self.__run_one_thread(cmd, back)
        else:
          self.__run_one(cmd, back)

      self.__soft_exit()

    except:
      # todo: recovery starts immediately after an error occurs
      self.__hard_exit()

    # warning, recoursive
    finally:
      if self.__back_after:
        self.__back_after = False
        self.run( back=True )
    pass

  def __run_one(self, cmd, back=False):

    line = cmd.line if not back else cmd.back

    if line:

      if cmd.info:
        print(('[recover]' if back else '') + cmd.info, end='', file=self.__stream)
        if self.__flush:
          self.__stream.flush()


      if cmd.ignore_errors:
        rt = Return.ignore
      elif cmd.only_errcode:
        rt = Return.errcode
      else:
        rt = Return.process
      result = x( command=line,
                  return_type=rt,
                  ignore_output=cmd.ignore_output,
                  thread_log_and_lock={'log':self.log, 'Lock':self.__lock},
                  pause=cmd.pause_before,
                  timeout=cmd.timeout )

      if not cmd.ignore_errors:
        if not cmd.only_errcode and result.returncode != OK:
          raise self.TerminalException
        if cmd.only_errcode and result != OK:
          raise self.TerminalException

  def __run_one_thread(self, cmd, back=False):

    line = cmd.line if not back else cmd.back

    if line:

      if cmd.ignore_errors:
        rt = Return.ignore
      elif cmd.only_errcode:
        rt = Return.errcode
      else:
        rt = Return.process
      self.__threads.append(
        Thread( target=x, args=[ line,
                                 rt,
                                 cmd.ignore_output,
                                 {'log':self.log, 'Lock':self.__lock},
                                 cmd.pause_before,
                                 cmd.timeout
                                ] ) )
      self.__threads[-1].daemon = True

      if cmd.info:
        print(('[recover]'if back else '') + cmd.info, end='', file=self.__stream)
        if self.__flush:
          self.__stream.flush()

      self.__threads[-1].start()
      time.sleep( 1 )

  def print_log(self):
    print('----------log-----------\n', end=' ', file=self.__stream)

    for out in self.log:
      print('{:4} | {}\n'.format( out[0], out[1] ), end=' ', file=self.__stream)

    pass

  def clear(self):
    self.__hard_exit()
    del self.__commands[:]
    del self.__threads[:]
    del self.log[:]
    del self.errors[:]

    self.__err_msg   = ''

    pass

  pass

class Ticker(object):
  """ Timer. It starts immediately after initialization.

      Parameters:
      timeout         (int, float) - sets timeout in seconds or fractions of seconds
      high_resolution (bool)       - use performance counter with highest available resolution
      precision       (int)        - round results to a given precision in decimal digits

  """

  def __init__(self, timeout=10, high_resolution=False, precision=0):
    self.__time_func = time.perf_counter if high_resolution else time.time
    self.__precision = precision

    self.timeout = timeout
    self.points = dict()
    self.first_point = 0.0
    self.last_point = 0.0

  def __sub__(self, other):
    if self.__precision > 0:
      return round(self.last_point - other.last_point, self.__precision)
    else:
      return self.last_point - other.last_point

  def __start(self):
    if self.__precision > 0:
      self.first_point = round(self.__time_func(), self.__precision)
    else:
      self.first_point = self.__time_func()
    self.points = { 'start' : self.first_point }

    return self

  def tick(self) -> bool:
    delta = self.__time_func() - self.first_point

    if delta < self.timeout:
      return True
    else:
      return False

  def tock(self) -> float:
    if self.__precision > 0:
      return round(self.__time_func() - self.first_point, self.__precision)
    else:
      return self.__time_func() - self.first_point

  def check(self, point_id:str):
    if point_id:
      if point_id not in ('start', 'stop'):
        self.points[ point_id ] = self.tock()

  def __enter__(self):
    return self.__start()

  def __exit__(self, type, value, trace):
    self.last_point = self.tock()
    self.points['stop'] = self.last_point

class ConPlace(object):

  CURSOR_UP     = '\x1b[1A'
  ERASE_LINE    = '\x1b[K'
  CLEAR_TO_END  = '\x1b[0J'

  __SAVE_CURSOR   = '\x1b[s'
  __LOAD_CURSOR   = '\x1b[u'

  def __init__(self, stream=sys.stdout, end_pause=1.0):
    """
      end_pause - seconds
    """
    self.stream = stream
    self.pause  = end_pause

  def do(self, action):
    """
      action - one of fields ConPlace-class
      Example: do( ConPlace.CURSOR_UP )
    """
    if action:
      self.stream.write( action )

  def __enter__(self):
    self.stream.write( self.__SAVE_CURSOR )
    self.stream.write( '\n' )
    return self

  def __exit__(self, type, value, trace):
    time.sleep( self.pause )
    self.stream.write( self.ERASE_LINE )
    self.stream.write( self.__LOAD_CURSOR )
    self.stream.write( self.CLEAR_TO_END )
    self.stream.flush()
    pass

class Context(NamedTuple):
  """
    u : selinux-user
    r : selinux-role
    t : selinux-type(or domain)
    s : array of available levels (selinux-mls)
    c : array of available categories (selinux-mсs)
    full : full security label(context) as string
  """

  str:str = "file_not_found"
  u:str   = "_"
  r:str   = "_"
  t:str   = "_"
  s:list  = []
  c:list  = []

  def update(self):
    s = self.__convert_arrays_to_context_ranges()
    c = self.__convert_arrays_to_context_ranges(True)

    self.full = '{}:{}:{}:{}{}'.format(
      self.u,
      self.r,
      self.t,
      s if s else 's0',
      ':' + c if c else ''
    )
    return self

  @staticmethod
  def sequence(array:List[int], strict=0, cut=False):
    length = len(array)

    if length == 1:
      return [array.pop()] if cut else array
    elif length == 1:
      return []

    src = array if cut else array[:]
    result = [src.pop(0)]

    while src:
      if result[-1] < src[0]:
        if strict and src[0] - result[-1] != strict:
          return result
        result.append(src.pop(0))
      else:
        return result

    return result

  @staticmethod
  def transform_to_char_range(array:List[int], category=False):
    """
      array (list of integers) : values must be strictly sequential,
                                 in other words, their order is not important,
                                 but they can be used to form a continuous
                                 non-decreasing sequence
                                 Example:
                                  [1, 5, 2, 4, 3] -> [1, 2, 3, 4, 5] OK
                                  [1, 6, 2, 4, 3] -> WRONG (6-4 > 1)
    """

    _min = _max = None

    for val in array:
      if _min is None or val < _min:
        _min = val
      if _max is None or val > _max:
        _max = val

    if category:
      if _min == _max:
        result = 'c{}'.format(_min)
      else:
        result = 'c{}.c{}'.format(_min, _max)
    else:
      if _min == _max:
        result = 's{}'.format(_min, _max)
      else:
        result = 's{}-s{}'.format(_min, _max)

    return result

  def __convert_arrays_to_context_ranges(self, category=False):
    transformed = []

    if category:
      array = self.c[:]

      while array:
        transformed.append(
          Context.transform_to_char_range(
            Context.sequence(array, 1, cut=True),
            category=category
          )
        )

      return ','.join( transformed )

    else:
      return Context.transform_to_char_range(self.s)

class User(object):

  U = None

  def __init__(self, name='', password='', need_home='', group='', root=False, system=False):
    self.U = User.create( name, password, need_home, group, root, system )

  def __del__(self):
    User.delete( self.U.name )

  @staticmethod
  def create(name='', password='', need_home='', group='', root=False, system=False):
    """
        Very DANGEROUS. Without compromise..

        return nametuple('user','name password uid gid gecos home shell')
    """

    if ( os.getuid() != 0 ):
      print("Need to run as superuser!", file=sys.stderr)
      exit(0)

    if not name:
      print("Username is empty! Exit.", file=sys.stderr)
      exit(1)

    flags = ''

    if system:
      flags += ' --system'

    if password:
      encrypted_password = crypt.crypt( password, '22' )
      flags += ' --password {}'.format( encrypted_password )
    else:
      flags += ' --no-log-init'

    if root:
      flags += ' --non-unique --uid 0 --gid 0'

    if need_home:
      flags += ' -d {}'.format( need_home )
    else:
      flags += ' -M'

    if group:
      flags += ' -G {}'.format( group )
    # print('useradd{} {}'.format( flags, name ))

    res = x('useradd{} {}'.format( flags, name ), return_type=Return.process )

    if res.returncode:
      print('userCreate error: {}'.format( res.stdout.read() ), file=sys.stderr)
      exit(res.returncode)

    if not password:
      res = x( 'passwd -f -u {}'.format( name ), return_type=Return.process )

      if res.returncode:
        print('userCreate:nopassword error: {}'.format( res.stdout.read() ), file=sys.stderr)
        exit(res.returncode)

    return User.getByName(name)

  @staticmethod
  def delete(name='', remove_files=True):
    """
        Very DANGEROUS. Without compromise..
    """

    flags = ' --remove' if remove_files else ''

    if ( os.getuid() != 0 ):
      print("Need to run as superuser!", file=sys.stderr)
      exit(0)

    if name:
      res = x( 'userdel -f{} {}'.format(flags, name), return_type=Return.process )

      if res.returncode:
        print('userDelete error: {}'.format( res.stdout.read() ), file=sys.stderr)
        exit(res.returncode)
    pass

  @staticmethod
  def exist(name=''):
    users = [struct[0] for struct in pwd.getpwall()]
    return name in users

  @staticmethod
  def getByUid(uid=500):
    users  = [ struct for struct in pwd.getpwall()]

    for u in users:
      if u[2] == uid:
        user = namedtuple( 'user','name password uid gid gecos home shell')
        user.name, user.password, user.uid, user.gid, user.gecos, user.home, user.shell = u
        return user

  @staticmethod
  def getByName(name='root'):
    users  = [ struct for struct in pwd.getpwall()]

    for u in users:
      if u[0] == name:
        user = namedtuple( 'user','name password uid gid gecos home shell')
        user.name, user.password, user.uid, user.gid, user.gecos, user.home, user.shell = u
        return user

  @staticmethod
  def current(root=False):
    if root:
      return User.getByUid( os.getuid() )
    else:
      name = x( 'logname', return_type=Return.stdout ).strip()
      return User.getByName( name )

  @staticmethod
  def selinux_getcon() -> Context:
    """
      fields:
        str:str, u:str, r:str, t:str, s:list, c:list
    """

    context_str = selinux.getcon_raw()[1]
    context_lst = context_str.split(':')

    if len(context_lst) == 5:
      return Context(
                      context_str,
                      context_lst[0],
                      context_lst[1],
                      context_lst[2],
                      rangeGetFromCon( context_str, mcs=False ),
                      rangeGetFromCon( context_str, mcs=True )
                    )

    if len(context_lst) == 4:
      return Context(
                      context_str,
                      context_lst[0],
                      context_lst[1],
                      context_lst[2],
                      rangeGetFromCon( context_str, mcs=False ),
                      None
                    )

class TestError(Exception):
  def __init__(self, message:str, code:int):
    super().__init__(message)
    self.code = code

class CleanException(Exception):
  def __init__(self, TestObject:object):
      super().__init__()
      self.object = TestObject

class SystemCtl(object):

  cmd     = 'systemctl'

  Service = namedtuple('Service', 'unit load active sub description')
  Process = namedtuple('Process', 'pid exec code status')
  Status  = namedtuple('Status',  'load path preset active since processes journal')

  def __init__(self, stream=sys.stdout):
    self.__stream = stream

  def start(self, service_name:str) -> int:
    cmd       = 'systemctl start {}'.format( service_name )
    code, out = x( cmd, Return.codeout )

    if code != 0:
      print( 'SystemCtl::start(): error {} - {}'.format( code, out ), file=self.__stream )
      return code

  def stop(self, service_name:str) -> int:
    cmd       = 'systemctl stop {}'.format( service_name )
    code, out = x( cmd, Return.codeout )

    if code != 0:
      print( 'SystemCtl::stop(): error {} - {}'.format( code, out ), file=self.__stream )
      return code

  def get_services(self, service_name='', all=False):

    cmd       = 'systemctl --type=service --full --plain --no-legend --no-pager{}'.format( ' --all' if all else '' )
    code, out = x( cmd, Return.codeout )

    if code != 0:
      print( 'SystemCtl::get_services(): error {} - {}'.format( code, out ), file=self.__stream )
      return

    services = []
    for line in out.splitlines():

      fields     = line.split( maxsplit=4 )
      fields[-1] = fields[-1].strip()

      if service_name and service_name.lower() in line.lower():
        return self.Service( *fields )

      else:
        services.append( self.Service( *fields ) )

    return services

  def status(self, service_name:str):

    cmd = 'systemctl status {} --no-pager'.format( service_name )
    code, out = x( cmd, Return.codeout )

    if code == 4:
      print( 'SystemCtl::status(): Unit {} could not be found'.format( service_name ), file=self.__stream )
      return None

    processes = []
    journal   = []

    pid = exe = code = status = main_pid  = ''
    load = path = preset = active = since = ''

    journal_begin = False
    for line in out.splitlines():
      line = line.lower()

      if journal_begin:
        journal.append( line )
        continue

      if '' == line:
        journal_begin = True
        continue

      elif 'process:' in line or 'main pid:' in line:
        # https://regex101.com/r/50OQ13/1/
        reg = re.findall( r'(?:(\d{1,6})(?: .*?=)?(.*) \(.*=(?:(.*), .*=(.*))\))|(\d{1,6})', line )
        if reg:
          pid, exe, code, status, main_pid = reg[0]
          process                          = self.Process( pid or main_pid, exe, code, status)
          processes.append( process )

      elif 'loaded:' in line:
        # https://regex101.com/r/LlIDXr/2
        # https://regex101.com/r/QfcT5p/1
        reg = re.findall( r'(?:Loaded: (\w*)) (?:(?:\()?(.*?);(?:(?:.*): (.*).*(?:\)))?)?', line, re.IGNORECASE )
        if reg:
          load, path, preset = reg[0]

      elif 'active:' in line:
        # https://regex101.com/r/ka3SW2/1
        # https://regex101.com/r/Jy1chn/1
        reg = re.findall( r'(?:Active: (\w*)(?:(?:.*since )(.*);)?)', line, re.IGNORECASE )
        if reg:
          active, since = reg[0]

    if not load or not active:
      print( 'SystemCtl::status(): looks like regular expressions need to be updated or service name is wrong.', file=self.__stream )
      return None

    status = self.Status( load, path, preset, active, since, processes, journal )

    return status

class QuotedPath(pathlib.PosixPath):

  def format(self, foreground: int = -1, background: int = -1, quoted=True, **kwargs):
    if quoted:
      return '"' + colored(f'{self.as_posix()}', foreground, background, **kwargs) + '"'
    else:
      return colored(f'{self.as_posix()}', foreground, background, **kwargs)

class Walker():
  """ This class allows you to go through all the files in the specified folders or count their number.
      For best performance, Walker uses its own cache to truncate stat() system calls
  """

  def __init__(self, include:Tuple[str], exclude:Tuple[str]=(), ignore_dirs=False) -> None:
    self.__included = include
    self.__excluded = exclude
    self.__ignore_dirs = ignore_dirs
    self.__stat_cache = dict()
    self.__count = 0

  def count(self, dirs_only=False):
    self.__count = 0
    for include in self.__included:
      for entry in self.__walk(include):
        if dirs_only and not stat.S_ISDIR(self.__stat_cache[entry].st_mode):
          continue
        if entry.path.startswith(self.__excluded):
          continue
        self.__count += 1
      yield include, self.__count

  def __walk(self, path:str):
    for entry in os.scandir(path):
      try:
        st = entry.stat(follow_symlinks=False)
        self.__stat_cache[entry] = st
        if stat.S_ISDIR(st.st_mode):
          yield from self.__walk(entry.path)
      except (PermissionError, FileNotFoundError):
        continue
      yield entry

  def __iter__(self):
    self.__stat_cache.clear()

    for include in self.__included:
      for entry in self.__walk(include):
        if stat.S_ISDIR(self.__stat_cache[entry].st_mode) and self.__ignore_dirs:
          continue
        if entry.path.startswith(self.__excluded):
          continue
        res = QuotedPath(entry)
        res.__cached_stat = self.__stat_cache[entry]
        res.stat = res.lstat = lambda : res.__cached_stat
        yield res

  def __next__(self):
    return next(self)


def x(command, return_type=Return.errcode, ignore_output=False, log_and_lock={'log': [], 'Lock': None}, pause=0, timeout=0):
  """ Python shell runner. This is a "simple" wrapper.
      log_and_lock = { [ [errcode, output], ...], multiprocessing.Lock() }

      example: print( x('echo 123', return_type=Return.stdout) )

      if return_type == Return.codeout then x returns tuple(errcode,stdout\stderr )
      if return_type == Return.stdout  then x returns str
      if return_type == Return.errcode then x returns int
      if return_type == Return.process then x returns object of subrocess.Popen(...)
      if return_type == Return.ignore  then x returns 0

      pause         (float) - make pause before executing, in seconds.
      timeout       (float) - seconds. if timed out, return errcode(1). default: not limited.
      ignore_output (bool)  - adds suffix '> /dev/null' to command.
  """

  suffix    = ''
  waskilled = False
  output    = tempfile.SpooledTemporaryFile( mode='w+', encoding='UTF-8' )

  LnL = log_and_lock if type( log_and_lock['Lock'] ) is classLock else None

  if not command:
    print( 'x: command is empty' )
    return 1

  if ignore_output or return_type == Return.errcode:
    suffix = ' > /dev/null'

  if pause > 0:
    time.sleep(pause)

  process = Popen( command + suffix, shell=True, universal_newlines=True, stdout=output, stderr=STDOUT )
  process.stdout = output

  if timeout > 0:
    timer = Ticker( timeout )
    while timer.tick() and process.poll() == None:
      pass

    if not timer.tick() and process.poll() == None:
      process.kill()
      process.returncode = 1
      waskilled = True
  else:
    process.wait()

  process.stdout.seek(0)
  retcode = process.returncode if not waskilled else 1

  if return_type != Return.errcode:
    if not waskilled:
      retout = process.stdout.read()
      process.stdout.seek(0)
      if LnL:
        retout = retout.replace('\n', ';') # if writing to log
    elif return_type == Return.stdout:
      retout  = 'Timed out or stdout overloaded!'


  if return_type == Return.stdout:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return retout

  if return_type == Return.codeout:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return retcode, retout

  elif return_type == Return.errcode:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, 'Command: ' + command ] )
    return process.returncode

  elif return_type == Return.process:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return process

  elif return_type == Return.ignore:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ OK, '[ignore errors] {}'.format( retout ) ] )
    return OK

def fileBackup(uri='', restore=False, replacement=''):

  backup = uri + '.bkp'
  path   = ''
  name   = ''

  if restore == False and not os.path.exists( uri ):
    raise Exception('There is nothing to back up. File {} not found!'.format( uri ))

  if replacement and os.path.exists( replacement ):
    path = os.path.split( uri )[0]
    name = os.path.split( replacement )[1]

  elif replacement and not os.path.exists( replacement ):
    raise Exception('Replacement does not exist. File {} not found!'.format( uri ))

  # "some_magic.was".[::-1].replace('was.','',1)[::-1]

  if restore:

    if os.path.exists ( backup ):

      if os.path.exists( uri ):
        os.remove( uri )

      os.rename( backup, uri )

    else:
      raise Exception('Backup of "{}" not found!'.format( backup ))

  else:

    if os.path.exists ( uri ):

      if os.path.exists( backup ):
        raise Exception('Backup file "{}" already exists!'.format( backup ))
      else:
        os.rename( uri, backup )

        if replacement:
          shutil.copy2( replacement, uri)

    else:
      raise Exception('File to save "{}" not found!'.format( uri ) )

  pass

def cls():
    os.system('clear')

def processEx(name, kill=False) -> int:
  """
      process found     - return pid
      process killed    - return 0
      process not found - return -1
      error             - return -2

  """
  for proc in psutil.process_iter():
    try:
      if name.lower() in proc.name().lower():
        if ( kill ):
          proc.kill()
        return proc.pid
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
      return -2

  return -1

def whatlibs(Exit=True):
    pid  = os.getpid() # awk '{print $NF}' after
    deps = check_output("lsof -p {} | awk {} | grep -v 'NAME' | grep '.so'" \
                                   .format(pid, "'{print $NF}'"), shell=True).splitlines()
    print("Dependencies: \n", file=sys.stdout)
    for d in deps:
        print(d, file=sys.stdout)

    if (Exit):
        exit(0)

def groupCreate(name=''):

  if not name:
    print('Groupname is empty! Exit.', file=sys.stderr)
    exit(1)

  flags = ' {}'.format( name )

  res = x( 'groupadd' + flags, return_type=Return.process )

  if res.returncode:
    print('groupCreate error: {}'.format( res.stdout.read() ), file=sys.stderr)
    exit(res.returncode)

  pass

def groupDelete(name=''):

  if not name:
    print('Groupname is empty! Exit.', file=sys.stderr)
    exit(1)

  flags = ' {}'.format( name )

  res = x( 'groupdel' + flags, return_type=Return.process )

  if res.returncode:
    print('groupDelete error: {}'.format( res.stdout.read() ), file=sys.stderr)
    exit(res.returncode)

  pass

def chownR(path='', user='', group=''):

  if not path or not user or not group:
    print("One of parameters is empty! Exit.", file=sys.stderr)
    exit(1)

  for root, dirs, files in os.walk( path ):
    for d in dirs:
      os.chown( os.path.join( root, d ),
                pwd.getpwnam( user ).pw_uid,
                grp.getgrnam( group ).gr_gid )

    for f in files:
      os.chown( os.path.join( root, f ),
                pwd.getpwnam( user ).pw_uid,
                grp.getgrnam( group ).gr_gid )

  pass

def chmodR(path='', rights=''):

  if not path or not rights:
    print("One of parameters is empty! Exit.", file=sys.stderr)
    exit(1)

  for root, dirs, files in os.walk( path ):
    for d in dirs:
      os.chmod( os.path.join( root, d ), int( rights, base=8) )

    for f in files:
      os.chmod( os.path.join( root, f ), int( rights, base=8) )

  pass

def timerBlockCountDown(msg='', prefix='', check_interval = 0.1, waiting = 10, unit='sec'):
  """
      return True if any printer is busy
      waiting - seconds
  """
  t = Ticker(waiting)

  with ConPlace() as CP:
    print(msg, file=sys.stdout)

    while ( t.tick() ):

      time.sleep( check_interval )
      tock = time.time()

      print( f'{prefix} {t.tock():.0f}/{waiting} {unit}', end='\r', file=sys.stdout )
      sys.stdout.flush()
    else:
      return True

  return False

def gen_sha512(phrase=''):
  return hashlib.sha512(phrase).hexdigest()

def input_(prompt, placeholder='') -> str:
  import readline

  readline.set_startup_hook( lambda: readline.insert_text( placeholder ) )
  try:
    return input(prompt)

  finally:
    readline.set_startup_hook()

def se_enforce():

  # static var
  if not hasattr(se_enforce, 'SE_ENFORCING_STATUS'):
    se_enforce.SE_ENFORCING_STATUS = selinux.security_getenforce()

  if se_enforce.SE_ENFORCING_STATUS != 1:
    if selinux.security_getenforce():
      selinux.security_setenforce( 0 )
      print( 'SElinux: отключение... [ теперь {} ]'\
                    .format( 'permissive' if selinux.security_getenforce()==0 else 'enforcing' ) )
    else:
      selinux.security_setenforce( 1 )
      print( 'SElinux: включение... [ теперь {} ]'\
                    .format( 'permissive' if selinux.security_getenforce()==0 else 'enforcing' ) )

def seMakeSuite(clear=False):

  if not clear:

    u = User.create('tester', need_home='/home/tester')
    User.selinux_link('tester', seuser='user_u', lvl_range='s0-s3:c1.c3')

    files = {
      '/home/tester/s0.txt'   : 'user_u:object_r:user_home_t:s0',
      '/home/tester/s3c1.txt' : 'user_u:object_r:user_home_t:s3:c1',
      '/home/tester/s3.txt'   : 'user_u:object_r:user_home_t:s3',
      '/home/tester/s2c4.txt' : 'user_u:object_r:user_home_t:s0:c4'
    }
    print(files)
    for path, con in files.items():
      with open( path, 'w' ) as f:
        f.write('text')
      selinux.setfilecon_raw( path, con )

  else:
    User.delete('tester')
    User.selinux_unlink('tester')

def rangeGetFromCon(context:str, mcs=False):

  if   not context:            return []
  elif not context[3]:         return []
  elif mcs and not context[4]: return []

  _ranges = context.split(':')[4] if mcs else context.split(':')[3]
  _ranges = _ranges.replace( 'c' if mcs else 's', '')
  _ranges = _ranges.split(',')


  int_ranges = []
  for rng in _ranges:

    if '.' in rng:
      sep = '.'
    elif '-' in rng:
      sep = '-'
    else:
      sep = ''

    if sep:
      rng = [ int(_) for _ in rng.split( sep ) ]
      rng = [ _ for _ in range( rng[0], rng[-1:][0]+1) ]
      int_ranges.extend( rng )

    else:
      int_ranges.append( int(rng) )

  int_ranges.sort()
  return int_ranges

def userConGet() -> Context:
  """
    fields:
      str:str, u:str, r:str, t:str, s:list, c:list
  """
  context_str = selinux.getcon_raw()[1]
  context_lst = context_str.split(':')

  if len(context_lst) == 5:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    rangeGetFromCon( context_str, mcs=True )
                  )

  if len(context_lst) == 4:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    None
                  )

def getcon(file:str) -> Context:
  P = Path( file )

  if P.is_symlink():
    P = Path( os.readlink( P.as_posix() ) )

  if not P.exists():
    return Context()

  try:
    context_str = selinux.getfilecon_raw( file )[1]
    context_lst = context_str.split(':')
  except:
    return Context()

  if len(context_lst) == 5:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    rangeGetFromCon( context_str, mcs=True )
                  )

  if len(context_lst) == 4:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    None
                  )

def colored( string:str, foreground:int=-1, background:int=-1,
              underline=False, bold=False, dim=False, italic=False, blink=False ) -> str:
  """ The function returns a string with added ansi colorcodes.

      Parameters:
      string     (str)  - source string to be colored
      foreground (int)  - 256-bit color code
      background (int)  - 256-bit color code

      Attributes:
      hasattr(colored, 'DISABLED') - if true, the function will not colorize the text.
                                     This can be useful when the text is being saved to
                                     a file where the escape sequences would be redundant.

                                     Example:
                                       >> colored('COLORED', 9)
                                       [38;5;9mCOLORED[0m

                                       >> colored.DISABLED = 1
                                       >> colored('COLORED', 9)
                                       COLORED

      Returns:
      (str) : always returns a string type
  """

  RESET = '\x1b[0m'
  res   = ''

  if hasattr(colored, 'DISABLED'):
    return str(string)

  content = str(string).splitlines()

  for part in content:

    if foreground > 0:
      buf = '\x1b[38;5;{fg}m{content}{reset}'.format( fg=foreground, content=part, reset=RESET  )
    else:
      buf = part + RESET

    if background > 0:
      buf = '\x1b[48;5;{bg}m'.format( bg=background ) + buf

    if underline:
      buf = '\x1b[4m' + buf

    if bold:
      buf = '\x1b[1m' + buf

    if dim:
      buf = '\x1b[2m' + buf

    if italic:
      buf = '\x1b[3m' + buf

    if blink:
      buf = '\x1b[5m' + buf

    if part != content[-1]:
      res += buf + '\n'
    else:
      res += buf

  return res

def exception_handler(exception_type, exception, traceback, default_hook=sys.excepthook):
  if exception_type.__name__ == 'TestError':
    print( f'[{exception.code}]: {exception}', file=sys.stderr )
  else:
    default_hook( exception_type, exception, traceback )

def created_files(root:Path, period:Tuple[float,float], entry_names=['**'], quiet=True, delete=False):
  """ The function allows you to determine which files and directories were created in a certain period of time.

      Parameters:
          root        (pathlib.Path)         - the directory being monitored.
          period      (Tuple[float, float])  - two time points between which files should appear,
                                               the time point can be taken with the time.time function.
          entry_names (List[str])            - specific paths to expected files, by default ** - any files;
                                               if entry_names are specified, only they will be monitored.
          quiet       (bool)                 - if True information for each found file will be displayed
          delete      (bool)                 - if True, an attempt will be made to delete the file.

      Returns:
      (List[str]) : returns a list of files created during period.
  """

  files = []
  start, stop = period

  for entry in list( root.glob('**/*') ) + [root]:
    try:
      if entry.exists():
        for name in entry_names:
          if name in entry.as_posix() or name == '**':

            try:
              create_file_time = entry.stat().st_mtime

              if create_file_time >= start and create_file_time <= stop:
                files.append( entry )

                if delete:
                  shutil.rmtree( entry.as_posix(), ignore_errors=True )

                if not quiet:
                  print( f'[{colored("deleted",208) if delete else "found"}] "{entry}" -> {create_file_time} in [{start},{stop}]' )

            except (PermissionError, FileNotFoundError) as e:
              continue
    except (PermissionError, FileNotFoundError) as e:
      continue

  return files

def check_depends( mod_name:str, pkg_name:str ) -> bool:

  process = run( f'rpm -q {pkg_name}', shell=True, universal_newlines=True, stdout=PIPE )
  code, _ = (process.returncode, process.stdout)

  if code == 1:

    if quest( '  Зависимость {} не обнаружена! Установить? '.format( pkg_name ), 'y' ):

      process = run( f'dnf install -y {pkg_name} --refresh', shell=True, universal_newlines=True,
                                                          stdout=sys.stdout, stderr=sys.stderr, stdin=sys.stdin )

      if process.returncode != 0:

        if quest( '  Во время установки зависимости возникла проблема, продолжить? ({variants}): ', variants='y|n' ) == 'n':
          exit(1)
        return False

    else:
      exit(0)

  elif code > 1:
    print( '  Во время проверки наличия зависимости возникла проблема, продолжить? ({variants}): ' )
    exit(1)

  globals()[mod_name] = importlib.import_module(mod_name)
  os.system( 'clear' )

  return True

def quest(question:str, prepared='', variants='') -> Union[bool, str]:
  """ answer is always case insensitive
      example: quest( 'Hello, ? {variants}', 'mike', 'Mike|Michael|Mihairu' )
  """

  variants = [ ans.strip().lower() for ans in variants.split('|') if ans != '' ]
  varmode  = False

  if '{variants}' in question:
    question = question.format( variants=','.join(variants) )

  if len(variants) > 0:
    if prepared and prepared not in variants:
      print( 'quest: prepared answer does not match any of variants' )
      return False

    varmode = True

  import readline

  readline.set_startup_hook( lambda: readline.insert_text( prepared ) )
  try:
    if varmode:

      variant = ''
      while variant not in variants:
        variant = input(question).strip().lower()

      return variant

    elif prepared in input(question).lower():
      return True

    else:
      return False

  except KeyboardInterrupt:
    return '' if varmode else False

  finally:
    readline.set_startup_hook()

def check_kernel_config(param_name:str, need_value='y|m', config = '') -> int:
  """
  return 0 - all is ok
  return 1 - kernel parameter has unexpected value
  return 2 - kernel parameter not found
  """

  if not config:
      for f in os.listdir( '/boot' ):
        if 'config' in f:
          config = '/boot/' + f
          break

  found = False
  with open( config, 'r' ) as kconf:
    for l in kconf.readlines():

      if l.startswith('#'): continue

      if param_name in l:
        found = True

        need_value = need_value.split('|')

        if l.strip('\n').split('=')[1] not in need_value:
          return 1
        else:
          break

  if not found:
    return 2
  else:
    return 0

def get_file_mode(entry) -> str:
  try:
    if isinstance(entry, Path):
      return oct( entry.lstat().st_mode & 0o7777 )[2:]
    else:
      return oct( entry.stat().st_mode & 0o7777 )[2:]
  except (PermissionError, FileNotFoundError) as e:
    return '000'

def profile(output:str):
  def decorator(func):
    def wrapper(*args, **kwargs):
      with cProfile.Profile() as profiler:
        rc = func(*args, **kwargs)

      data = io.StringIO()
      profiler_stats = pstats.Stats(profiler, stream=data)
      profiler_stats.sort_stats(pstats.SortKey.CUMULATIVE)
      if output:
        profiler_stats.dump_stats(output)
      else:
        profiler_stats.print_stats()
      return rc
    return wrapper
  return decorator

if __name__ == '__main__':
    # whatlibs()
    #cls()

    # with Ticker() as timer:
    #   for p in Walker(['/'], topdown=True):
    #     pass#print(f'{p}')
    # print(f'files: 000000', end=' ')
    # print(f'{timer.tock():>9.5f}', flush=True)

    # with Ticker() as timer:
    #   for p in Walker(['/']):``
    #     pass#print(f'{p}')
    # print(f'files: 000000', end=' ')
    # print(f'{timer.tock():>9.5f}', flush=True)

    # with Ticker() as timer:
    #   root, count = next(Walker(['/']).count())
    #   print(f'files: {count}', end=' ')
    # print(f'{timer.tock():>9.5f}', flush=True)

    # with Ticker() as timer:
    #   for entry,count in Walker(['/'], topdown=True).count():
    #     print(f'files: {count}', end=' ')
    # print(f'{timer.tock():>9.5f}', flush=True)

    # q = QuotedPath('/home/test/newrst.stat')
    # p = PosixPath('/var/log/audit')
    # print(p, f'{p}', p.as_posix())
    # print(q, f'{q}', q.as_posix())
    # print(p.exists())
    # print(q.exists())
    # print(q.format(underline=True))
    # stats = q.stat()
    # print(stats.st_gid, stats.st_uid)
    # path, files_count = next(Walker(('/'), ('/tmp', '/home', '/proc')).count(True))
    # print(path, files_count)
    pass

