#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import selinux
from sys import stderr

class CommonTest(Test):
  name = 'Общая проверка режима безопасности'

  def go(self, **kwargs):
    if 1 != selinux.is_selinux_enabled():
      print( 'SElinux выключен!', file=stderr )
      return 1

    if 1 != selinux.security_getenforce():
      print( 'SElinux в режиме permissive!', file=stderr )
      return 2

    if 1 != selinux.is_selinux_mls_enabled():
      print( 'SElinux не включена поддержка MLS!', file=stderr )
      return 3

    return 0

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    pass