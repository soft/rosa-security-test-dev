#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import pwd
import os

from typing import List
from kplpack.utils import User
from kplpack.utils import getcon
from kplpack.utils import created_files
from kplpack.utils import colored as I
from kplpack.utils import QuotedPath as Path

from shutil import copy, copytree


class TestObject:

  root  = None
  dirA  = None
  dirB  = None
  fileA = None
  fileB = None

  def __init__(self, root:Path):

    if root.exists():
      self.root  = root
      self.dirA  = Path(f'{self.root}/dirA')
      self.dirB  = Path(f'{self.root}/dirB')
      self.fileA = self.dirA / 'test.txt'
      self.fileB = self.dirB / 'test.txt'

  @staticmethod
  def check_mls_mapping(roots:List[Path]):

    user_context = User.selinux_getcon()

    for root in roots:
      try:

        if root.exists():
          for entry in root.glob('*/**'):

            try:
              if entry.exists() and not entry.is_symlink(): # and not entry.name.startswith('.'):

                context = getcon(entry.as_posix())

                if entry.is_dir():
                  if context.t != 'home_t' and context.t != 'user_home_dir_t' :
                    raise error(f'Некорректный контекст директории для {entry.format(underline=True)}: '
                                f'{context.t} != user_home_dir_t или home_t.', 1)

                if entry.is_file():
                  if context.t != 'home_t':
                    raise error(f'Некорректный контекст {context.str} файла для {entry.format(underline=True)}: '
                                f'{context.t} != home_t.', 2)

                if context.u != user_context.u:
                  log.warning(f'Некорректный пользователь для {entry.format(underline=True)}: '
                              f'{context.u} != {user_context.u}.')

                # user can only have one level during a session
                if context.s[0] > user_context.s[0]:
                  raise error(f'Некорректный уровень для {entry.format(underline=True)} : '
                              f'{user_context.u} с уровнем s{user_context.s[0]} не имеет к нему доступ.', 4)

                if context.c and not entry.is_dir():
                  if user_context.c:
                    for category in context.c:
                      if category not in user_context.c:
                        raise error(f'Некорректная категория для {entry.format(underline=True)}: '
                                    f'категория {category} не в ходит в доступные для {user_context.u}.', 5)
                  else:
                    raise error(f'Некорретная категория для {I(entry.quoted(), underline=True)}: '
                                f'объект имеет категории {context.c}, пользователь - нет.', 6)

            except (PermissionError, FileNotFoundError):
              print(I('Нет доступа', 110) + f' к {entry.format(underline=True)}. Пропускаем...')
              continue

      except (PermissionError, FileNotFoundError):
        print(I('Нет доступа', 110) + f' к {entry.format(underline=True)}. Пропускаем...')
        continue

  def create_stage(self):

    print(f'Проверка возможности создания файлов и директорий для {self.root.format(underline=True)}.')

    try:
      self.dirA.mkdir(parents=False)
      self.dirB.mkdir()
      self.fileA.touch()

    except (PermissionError, FileNotFoundError) as e:
      raise error(f'Ошибка создания директории и файла:\n  "{self.dirA}"\n  "{self.dirB}"\n '
                  f' "{self.fileA}"\n  {e}', 8)

    self.check_mls_mapping([self.dirA, self.dirB, self.fileA])

  def copy_stage(self):

    print(f'Проверка возможности копирования файлов и директорий для {self.root.format(underline=True)}.' )

    try:
      copy(self.fileA.as_posix(), self.fileB.as_posix())
      copytree(self.dirA.as_posix(), (self.dirB/self.dirA.name).as_posix())

    except (PermissionError, FileNotFoundError) as e:
      raise error(f'Ошибка копирования директории или файла:\n  "{self.dirA}"\n  "{self.dirB}"\n '
                  f' "{self.fileA}"\n  {e}', 9)

    self.check_mls_mapping([self.dirA, self.dirB])

  def remove_stage(self):

    print(f'Проверка возможности удаления файлов и директорий для {self.root.format(underline=True)}.')

    try:
      self.fileA.unlink()
      self.fileB.unlink()
      ( (self.dirB / self.dirA.name) / self.fileA.name ).unlink()
      (self.dirB / self.dirA.name).rmdir()
      self.dirA.rmdir()
      self.dirB.rmdir()

    except (PermissionError, FileNotFoundError) as e:
      raise error(f'Ошибка удаления директории или файла:\n  "{self.dirA}"\n  "{self.dirB}"\n '
                  f'"{self.fileA}"\n  {e}', 10)

class FileOperationsWithMLS(Test):

  name = 'Поддержка MLS при операциях над файлами'
  exclusive = False

  def go(self, **kwargs):
    root = Path.home()
    user = pwd.getpwuid( os.getuid() ).pw_name

    if not root.exists():
      raise error(f'Корневая директория для проверки не доступна: {root.format(underline=True)} .', 11)

    for entry in root.glob('*'):
      if entry.owner() != user:
        continue
      try:
        if entry.exists() and entry.is_dir():
          test = TestObject(entry)
          test.create_stage()
          test.copy_stage()
          test.remove_stage()
      except (PermissionError, FileNotFoundError):
        continue

    TestObject.check_mls_mapping([root])

    return 0

  def clean(self, **kwargs):
    print('Удаление побочных файлов:')
    root = Path.home()
    since = (kwargs.get('start_time'), kwargs.get('end_time'))

    deleted = created_files(root, since, ['dirA', 'dirB', 'text.txt'], delete=True, quiet=False)
    if len(deleted) == 0:
      print('  нечего удалять.')

  def duration(self, **kwargs):
    pass

