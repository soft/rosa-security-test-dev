#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

import logging
log = logging.getLogger("rst")

from kplpack.utils import Ticker
from kplpack.utils import colored as I

class TestError(Exception):
  def __init__(self, message:str, code:int):
    super().__init__(message)
    self.code = code

class CleanException(Exception):
  def __init__(self, TestObject:object):
      super().__init__()
      self.object = TestObject

class Test:

  name = ''
  path = ''
  mission = ''

  # for gui
  row = -1

  progress = 1
  progress_max = 1

  module = None
  exclusive = False

  def start(self, UI=None):
    with Ticker() as timer:

      try:
        if self.module:
          print( f'Запуск теста "{self.name}"' )
          return( self.go(ui=UI) )
        else:
          raise Exception('Модуль для этого теста не был подключен.')

      except TestError as e:
        log.error( f'[{e.code}]: {e}' )
        return e.code # see exception_handler()

      except OSError as e:
        log.error( f'Во время теста "{ self.name }" произошла ошибка "{ e }".' )
        return 131 + e.errno

      finally:
        print( f'Тест "{self.name}" завершен за ' + I( round(timer.tock(), 3), 150) + ' сек.' )
        self.clean( start_time=timer.first_point, end_time=timer.first_point + timer.tock() )

  def estimate(self):
    estimate = self.progress_max - self.progress
    if estimate < 0:
      log.error(f'Для теста {self.name} неправильно посчитан прогресс. Сообщите разработчику.')
      return 0
    return estimate

  def clean(self, **kwargs):
    raise NotImplementedError('Опредление метода clean(self, **kwargs) необходимо для каждого теста.')

  def duration(self):
    raise NotImplementedError('Опредление метода duration(self) необходимо для каждого теста.')

  def go(self, **kwargs):
    raise NotImplementedError('Опредление метода go(self, **kwargs) необходимо для каждого теста.')


if __name__ == "__main__":
  pass