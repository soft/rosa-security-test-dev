#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

# ┌──────────────────────────────────────────────────────────────────────────┐
# │                                                                          │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                          │
# │                           ROSA Security Test                             │
# │                                                                          │
# │   Copyright © 2020 LLC "NTC IT ROSA"                                     │
# │   License: GPLv3                                                         │
# │   Authors:                                                               │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2020            │
# │                                                                          │
# │   This program is free software; you can redistribute it and/or modify   │
# │   it under the terms of the GNU General Public License as published by   │
# │   the Free Software Foundation; either version 3, or (at your option)    │
# │   any later version.                                                     │
# │                                                                          │
# │   This program is distributed in the hope that it will be useful,        │
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of         │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           │
# │   GNU General Public License for more details                            │
# │                                                                          │
# │   You should have received a copy of the GNU General Public License      │
# │   License along with this program; if not, write to the                  │
# │   Free Software Foundation, Inc.,                                        │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.          │
# │                                                                          │
# ╘══════════════════════════════════════════════════════════════════════════╛

__version__ = '3.2.9'
__project__ = 'rosa-security-test'

import os

CURRENT_DIR   = os.path.dirname(os.path.abspath(__file__))
INSTALLED_DIR = 'none'

import init
import logging

import sys
import inspect
from importlib.util import spec_from_file_location
from importlib.util import module_from_spec

from pathlib import Path
from kplpack.utils import colored as I
from kplpack.utils import Walker
from TestClass import Test

log = logging.getLogger("rst")

def run_nogui():
  failed    = 0
  completed = 0

  tests = find_all_tests()

  if not tests:
    log.error('Ни одного теста не обнаружено!')
    return 1

  for num, test in enumerate( tests ):

    err = test.start()

    if err > 0:
      failed += 1
      print( f'Тест "{test.name}" {I("провален", 9)}! \n' )

      if ARGS.faultcode:
        return num + 1

    else:
      print( f'Тест "{test.name}" успешно {I("пройден", 10)}.\n' )
      completed += 1

  print( f'Тестирование завершено. Пройдено: {completed}, Провалено: {failed} .')

  if len( tests ) == completed:
    return 0
  else:
    return failed

def find_all_tests( search_dir:str= INSTALLED_DIR + '/tests/' ):

  # test = namedtuple( 'test','path type name')
  if INSTALLED_DIR == 'none':
    search_dir = CURRENT_DIR + 'tests/'
  tests = [ ]

  for entry in Walker([search_dir], ignore_dirs=True):
    if entry.stem.startswith('_'): continue

    mission = entry.parent.stem
    path = entry

    print( f'Подключение модуля : {path}')

    spec = spec_from_file_location( f'{path.stem}', path )
    module = module_from_spec( spec )
    sys.modules[path.stem] = module
    spec.loader.exec_module(module)

    print(f'Извлечение тестового класса...')

    test_object:Test = None
    for name,obj in module.__dict__.items():
      if inspect.isclass(obj) and issubclass(obj, Test) and name != 'Test':
        print(f'Класс извлечён: {name}.')
        test_object = obj
        break

    if not test_object:
      log.error(f'Модуль {path} не содержит реализации класса Тест')
      continue

    test_object = test_object()
    test_object.mission = mission
    test_object.path = entry
    test_object.module = module

    if test_object.exclusive:
      exclusive_tests = [test_object]
      print( 'Режим EXCLUSIVE, будет запущен только тест c параметром "exclusive=True"!' )
      return exclusive_tests
    elif test_object:
      tests.append(test_object)

  return tests

def force_exit():
  if 'DISPLAY' in os.environ.keys():
    os.system( 'pkill -TERM -u `logname`' )
  else:
    os.system( 'pkill -KILL -u `logname`' ) # if tty

def is_live() -> bool:

  cmdline = Path( '/proc/cmdline' )

  if cmdline.exists():

    with cmdline.open( 'r', encoding='UTF-8' ) as f:
      line = f.read()

      if 'live:' in line or 'rd.live' in line:
        return True

  return False


if __name__ == "__main__":

  # get args from global var
  init.init()

  global ARGS
  ARGS = __builtins__.ARGS

  #os.system('clear')

  if ARGS.log_type in [ 'file', 'journal', 'std' ]:
    sys.stdout = init.StreamToLog( log )
    sys.stderr = init.StreamToLog( log, logging.ERROR )

  if ARGS.log_type == 'nolog':
      with open(os.devnull, 'ab', 0) as stdout:
        os.dup2( stdout.fileno(), sys.stdout.fileno() )

      with open(os.devnull, 'ab', 0) as stderr:
        os.dup2( stderr.fileno(), sys.stderr.fileno() )

  print( f'rosa-security-test[{__version__}] started from "{CURRENT_DIR}"' )

  if is_live():
    print( 'В режиме live проверка отключена', file=sys.stderr )
    exit(1)

  if os.getuid() == 0:
    print( 'Тесты НЕ должны быть запущены с правами администратора!', file=sys.stderr )
    exit(1)

  if not ARGS.gui:
    res = run_nogui()
    if ARGS.ultimate and res != 0:
      force_exit()

  else:
    from gui.GUI import *
    res = start(ARGS)

  logging.shutdown()

  exit(res)