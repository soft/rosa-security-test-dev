#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import os
import grp
#import psutil
import time


from kplpack.utils import Context, colored as I
from kplpack.utils import get_file_mode
from kplpack.utils import getcon
from kplpack.utils import QuotedPath
from kplpack.utils import Walker

class FilePermissions(Test):
  """
  НЕ проверяются симлинки
  НЕ проверяются сокеты
  НЕ проверяется /proc т.к. он не содержит объектов которые "реально" поддерживают хранение
     информации, а его регулярная проверка займёт очень много времени
  # Настройка /proc в /etc/fstab:
  #   /proc:proc  /proc proc  defaults,noexec,nodev,nosuid,hidepid=2  0 0
  #   позволяет скрывать из /proc процессы не принадлежащие пользователю.
  # Можно дать права пользователю на чтение всех процессов из /proc, добавив для него группу
  #   groupadd -g 1500 monitoring
  НЕ проверяется /var/tmp т.к. защищается MLS которая проверяется другим тестом (DeniedFileDirAccess)
  """

  name = 'Проверка границ пользовательского пространства'
  exclusive = False

  # settings
  exclude_dirs = (
    '/tmp', # mls
    '/var/tmp', # mls
    '/run/user', # mls
    '/home', # mls
    '/proc', # not a priority
    '/sys', # not a priority
    '/dev', # not a priority
    '/media', # mls
    '/mnt' # mls
  )
  exclude_groups = { 10 } # wheel, cat /etc/group
  exclude_objects = (
    '/dev/urandom',
    '/dev/random',
    '/dev/zero',
    '/dev/null',
    '/dev/dri/renderD128',
    '/dev/full',
    '/dev/mquene', # write only
    '/var/spool/postfix/dev/urandom' # not readable
  )
  exclude_context = (
    'var_run_t', # mls
    'cupsd_rw_etc_t' # mls
  )

  def object_type(self, entry:QuotedPath):
    obj_type = ''
    if entry.is_dir():
      obj_type = 'каталог'
    elif entry.is_block_device():
      obj_type = 'блочное устройство'
    elif entry.is_char_device():
      obj_type = 'символьное устройство'
    else:
      obj_type = 'файл'
    return obj_type

  def is_excluded_type(self, con:Context, entry:QuotedPath, obj_type:str):
    if con.t in self.exclude_context:
      log.warn(f'Объект {entry.format(underline=True)} имеет тип {con.t} исключённый из проверки, пропускаем...')
      return True
    return False

  def go(self, **kwargs):
    self.ui = kwargs.get('ui')

    current_user_login = os.getlogin()
    current_user_uid = os.getuid()
    current_user_groups = {grp.getgrgid(g).gr_gid for g in os.getgroups()}


    # print(f'Проверка параметров системы /proc...')
    # for mountpoint in psutil.disk_partitions(all=True):
    #   if mountpoint.device == 'proc':
    #     if 'hidepid=2' in mountpoint.opts:
    #       print(f'Параметр hidepid=2 установлен для /proc.')
    #     else:
    #       raise error(f'Параметр hidepid=2 не установлен для /proc!', 1)

    # exclude from checking
    for g in self.exclude_groups:
      if g in current_user_groups:
        current_user_groups.remove(g)

    time.sleep(1)
    errors = 0
    i = 0

    for i,entry in enumerate(Walker(['/'], self.exclude_dirs), start=1):

      if entry.as_posix().startswith(self.exclude_objects):
        log.warn(f'Объект {entry.format(underline=True)} исключен из проверки.')
        continue

      if entry.is_symlink():
        continue

      try:
        if entry.is_socket():
          log.warn(f'Объект {entry.format(underline=True)} является сокетом, пропускаем...')
          continue
      except (PermissionError, FileNotFoundError) as e:
        continue

      # need to work on the architecture. test should not have access to the gui
      if self.ui and not i % 100:
        estimate = self.progress_max - self.progress
        if estimate > 100:
          self.progress += 100
          self.ui.action( 'progress_bar|inc', 100 )
        else:
          self.progress += estimate
          self.ui.action( 'progress_bar|inc', estimate )

      try:
        if entry.exists():
          mode = get_file_mode(entry)
          owner = entry.stat().st_uid
          group = entry.stat().st_gid
          obj_type = self.object_type(entry)
          log.info(f'{i}: [{mode} {owner}:{group}] {entry.format(underline=True)}')

          if owner == current_user_uid:
            selinux_con = getcon(entry.as_posix())

            if self.is_excluded_type(selinux_con, entry, obj_type):
              continue

            log.error(f'[{selinux_con.str + "]":40} {entry.format(underline=True):} ({obj_type}) : '
                      f'{current_user_login} - владелец!')
            errors += 1

          elif group in current_user_groups and mode[-2] in '2,3,6,7':
            selinux_con = getcon(entry.as_posix())

            if self.is_excluded_type(selinux_con, entry, obj_type):
              continue

            log.error(f'[{selinux_con.str + "]":40} {entry.format(underline=True)} ({obj_type}) : '
                      f'{current_user_login} имеет доступ через группу {entry.group()}!')
            errors += 1

          elif mode[-1] in '2,3,6,7':
            selinux_con = getcon(entry.as_posix())

            if self.is_excluded_type(selinux_con, entry, obj_type):
              continue

            log.error(f'[{selinux_con.str + "]":40} {entry.format(underline=True)} ({obj_type}) : '
                      f'доступ {mode} !')
            errors += 1

      except (OSError, KeyError, PermissionError):
        log.info(f'[{getcon(entry.as_posix()).str + "]":40} {entry.format(underline=True)} ({obj_type}) : '
                 f'удалось получить информацию, пропускаем...')

    if errors:
      log.error(f'Кол-во обнаруженных нарушений доступа: {I(str(errors), 9)} из {i} объектов.')
    return errors

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    path, files_count = next(Walker(('/'), self.exclude_dirs).count())
    return files_count
