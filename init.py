#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

# python38-systemd
# python38-dbus
# python38-libselinux
# python38-setuptools

import logging
import argparse
import argcomplete

from rst import __project__, __version__
from systemd.journal import JournalHandler
from kplpack.utils import colored as I
from kplpack.utils import QuotedPath as Path
from sys import stdout

# register global var
__builtins__['ARGS'] = None


def helpmsg(available_styles):

  if available_styles is None:
    available_styles = 'нет доступных стилей'
  else:
    available_styles = ', '.join(available_styles)

  print( f'\n{I(__project__, 33)} [{__version__}] - функциональный тест системы безопасности.' )

  message = \
  f"""
  Общее:
    -h --help           Показать это сообщение и завершить работу.
    -g --with-interface Запуск тестирования c графическим интерфейсом.
    -u --ultimate-mode  Ультимативный режим запуска: при ошибках пользователь имеет право
                        только на завершение сессии.
    --fault-code        Возвращать вместо кол-ва не пройденных тестов номер первого проваленного теста.
    --style             Работает только с параметром -g. Запуск интерфейса с заданной графической темой.
                        Доступные стили: {available_styles}.

  Настройка логирования:

    -l --log-type [nolog, file, journal, std]
                        "nolog"   без ведения лога;
                        "file"    вести лог в файл см. --log-path;
                        "journal" вести лог в journald;
                        "std"     вести лог в stdout\stderr (по-умолчанию).

    --log-level [error, warning, info, debug]
                        "error"   регистрировать только ошибки;
                        "warning" регистрировать ошибки и предупреждения (по-умолчанию);
                        "info"    регистрировать ошибки, предупреждения, информационные сообщения;
                        "debug"   регистрировать все события включая отладочный вывод.

    -p --log-path       Указывает каталог или файл в который нужно сохранять лог.
                        По-умолчанию: --log-path /var/log. Указывать --log-type file необязательно.
    -c --no-colors      Не вставляет ANSI-коды {I('подкрашивающие',219)} текст. Будет полезно, если лог
                        впоследствии будет читаться не в терминале.
  """

  print( message )
  print( 'kernelplv@gmail.com // m.mosolov.rosalinux.ru // www.rosalinux.ru 2020©' )

def setup_autocomplete(parser: argparse.ArgumentParser):
  argcomplete.autocomplete(parser)
  bashrc = Path('~/.bashrc').expanduser()
  register_string = 'eval "$(register-python-argcomplete rst)"'

  if bashrc.exists():
    autocomplete_on = False

    with bashrc.open('r') as f:
      for l in f.readlines():
        if register_string in l:
          autocomplete_on = True

    if not autocomplete_on:
      with bashrc.open('a') as f:
        f.write(register_string + '\n')

def init():

  available_styles = None
  try:
    from PyQt5.QtWidgets import QStyleFactory
    available_styles = QStyleFactory.keys()
  except ImportError:
    pass

  parser = argparse.ArgumentParser( add_help=False, usage=argparse.SUPPRESS )

  parser.add_argument( '-h', '--help', action='store_true' )
  parser.add_argument( '-g', '--with-interface', dest='gui', action='store_true' )
  parser.add_argument( '-u', '--ultimate-mode',  dest='ultimate', action='store_true' )
  parser.add_argument( '-l', '--log-type', dest='log_type', type=str, choices=[ 'nolog', 'file', 'journal', 'std' ], default='std' )
  parser.add_argument( '--log-level', dest='log_level', type=str, choices=[ 'error', 'warning', 'info', 'debug' ], default='warning')
  parser.add_argument( '-p', '--log-path', metavar='path', dest='log_path', type=str, default='' )
  parser.add_argument( '-c', '--no-colors', dest='no_colors', action='store_true', default=False)
  parser.add_argument( '--fault-code',  dest='faultcode', action='store_true' )
  parser.add_argument( '--style', dest='style', default='gtk2', choices=available_styles)

  setup_autocomplete(parser)
  args = parser.parse_args()
  __builtins__['ARGS'] = args

  if args.help:
    helpmsg(available_styles)
    exit( 0 )

  if args.no_colors:
    I.DISABLED = 1

  log_t = args.log_type
  log_level = args.log_level

  if log_t == 'nolog': pass
  else:
    log = logging.getLogger('rst')

    if log_level == 'debug':
      level = logging.DEBUG
      formatter = logging.Formatter( '%(asctime)s - %(levelname)s - %(message)s', '%H:%M:%S' )
    elif log_level == 'info':
      level = logging.INFO
      formatter = logging.Formatter( '%(levelname)s - %(message)s' )
    elif log_level == 'warning':
      level = logging.WARNING
      formatter = logging.Formatter( '%(levelname)s - %(message)s' )
    elif log_level == 'error':
      level = logging.ERROR
      formatter = logging.Formatter( '%(levelname)s - %(message)s' )

    if log_t == 'file' or args.log_path != '':

      if args.log_path == '':
        path = Path('/var/log')
      else:
        log_t = 'file'
        path = Path(args.log_path)

      try:
        if path.is_dir():
          name = Path(f'{__project__}[{__version__}].log')
          path = path / name
          fHandler = logging.FileHandler( path.as_posix(), 'w' )
        else:
          fHandler = logging.FileHandler( path.as_posix(), 'w' )

      except PermissionError as e:
        print(f'Нет доступа к {path.format(underline=True)}! Попробуйте другое расположение.')
        exit(1)

    if log_t == 'journal':
      formatter = logging.Formatter( '%(levelname)s - %(message)s' )
      fHandler  = JournalHandler(SYSLOG_IDENTIFIER='RST')

    if log_t == 'std':
      fHandler = logging.StreamHandler( stdout )

    logging.addLevelName( logging.ERROR,   I( 'ERR',   9 ) )
    logging.addLevelName( logging.WARNING, I( 'WRN', 183 ) )
    logging.addLevelName( logging.INFO,    I( 'INF',  24 ) )
    logging.addLevelName( logging.DEBUG,   I( 'DBG',  92 ) )

    log.setLevel( level )
    fHandler.setLevel( level )
    fHandler.setFormatter( formatter )
    log.addHandler( fHandler )

class StreamToLog(object):
  """
  Fake file-like stream object that redirects writes to a logger instance.
  example: sys.stderr = StreamToLogger(logger, logging.DEBUG)
  """
  def __init__(self, logger, log_level=logging.INFO):
    self.logger = logger
    self.log_level = log_level
    self.linebuf = ''

  def write(self, buf):
    for line in buf.rstrip().splitlines():
        if 'Gtk-WARNING' in line:
          continue
        else:
          self.logger.log(self.log_level, line.rstrip(), )

  def flush(self):
    pass
