#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

import dbus
import logging
import operator
import os
import sys
import time
import traceback

from pathlib import Path
from rst import find_all_tests
from TestClass import Test
from typing import List


from PyQt5 import QtCore
from PyQt5 import sip
from PyQt5.QtCore import QObject, QRunnable, QThreadPool, pyqtSignal, pyqtSlot, QSize
from PyQt5 import uic

from PyQt5.QtCore import QAbstractTableModel, QCoreApplication, QEvent, QModelIndex, Qt
from PyQt5.QtGui import QColor, QPainter
from PyQt5.QtWidgets import ( QApplication, QLabel, QMainWindow, QProgressBar,
                              QPushButton, QStyle, QStyledItemDelegate,
                              QStyleOptionButton, QStyleOptionViewItem,
                              QTableView, QDesktopWidget )

log = logging.getLogger("rst")

def tray_msg(title='title', msg='Hello', msg_type='dialog-warning', timeout=1):

  bus   = dbus.SessionBus()
  proxy = bus.get_object('org.freedesktop.Notifications', '/org/freedesktop/Notifications')

  proxy.Notify(                                            # dbus signatures
                'rosa-security-test',                      # s
                dbus.UInt32(0),                            # u
                msg_type,                                  # s
                title,                                     # s
                msg,                                       # s or as
                [''],                                      # as
                { '' : dbus.Byte(0,variant_level=1) },     # a{sv}
                timeout                                    # i
              )

class BlackWindow(QMainWindow):

  def __init__(self):
    super().__init__()

    self.__path = Path( os.path.realpath( __file__ ) ).parent.joinpath( 'blackout.ui' )
    if not self.__path.exists():
      sys.exit( -1 )

    uic.loadUi( self.__path.as_posix(), self )
    self.setAttribute( Qt.WA_TranslucentBackground )
    self.setAttribute( Qt.WA_NoSystemBackground )
    self.setWindowFlags( Qt.Window | Qt.FramelessWindowHint )
    self.setWindowState( Qt.WindowFullScreen )

class MainWindow(QMainWindow):

  def __init__(self):
    super().__init__()

    self.__path = Path( os.path.realpath( __file__ ) ).parent.joinpath( 'view.ui' )
    if not self.__path.exists():
      sys.exit( -1 )

    self.inner :QMainWindow = uic.loadUi( self.__path.as_posix() )

    self.inner.installEventFilter( self )
    self.inner.setWindowTitle( "ROSA Security Test" )
    self.inner.setFocusPolicy( Qt.StrongFocus )

    width  = QDesktopWidget().availableGeometry(self.inner).width()
    height = QDesktopWidget().availableGeometry(self.inner).height()

    if width <= 800 and height <= 600:
      self.inner.resize(QSize(width * 0.7, height * 0.7))
    else:
      self.inner.setFixedSize( QSize(640, 600) )

    self.__getFocus()
    self.__exit   = True
    self.__logout = False

  def __getFocus(self):
    self.inner.setFocus( True )
    self.inner.activateWindow()
    self.inner.raise_()
    self.inner.showNormal()

  def eventFilter(self, obj, event):
    if obj is self.inner and event.type() == QEvent.Close:
      if self.__exit:
        self.inner.removeEventFilter(self)
        self.exit()
      event.ignore()
      return True

    if event.type() == QEvent.FocusOut or event.type() == QEvent.WindowStateChange:
      self.__getFocus()

    if event.type() == QEvent.KeyRelease and event.key() == Qt.Key_Escape:
      if self.__exit and not self.__logout:
        self.inner.removeEventFilter(self)
        self.exit()

    return super(MainWindow, self).eventFilter(obj, event)

  def get(self) -> QMainWindow:
    return self.inner

  def setLogoutExit(self, val:bool=False):
    self.__logout = val

  def setExit(self, val:bool=True):
    self.__exit = val

  @pyqtSlot()    #@Slot() #pyside2
  def exit(self):
    self.inner.removeEventFilter(self)

    if self.__logout:
      os.system( 'pkill -TERM -u `logname`' )

    QCoreApplication.quit()

  def show(self, frameless:bool = False, noclose:bool = False):

    self.inner.setWindowFlags( Qt.SubWindow |
                               Qt.WindowStaysOnTopHint |
                               Qt.CustomizeWindowHint |
                               Qt.WindowCloseButtonHint )

    # disable minimize & maximize buttons
    self.inner.setWindowFlags( self.inner.windowFlags() & ~Qt.WindowMinimizeButtonHint )

    if frameless:
      self.inner.setWindowFlags( Qt.SubWindow | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint )
    if noclose:
      self.__exit = False

    self.inner.show()

class ProgressBar(object):

  def __init__(self, root):
    super().__init__()
    self.inner :QProgressBar = root.findChild( QProgressBar, "progress_bar" )

  def fill(self):
    self.inner.setValue( self.inner.maximum() )

  def set(self, val:int):
    self.inner.setValue( val )

  def setMax(self, val:int):
    self.inner.setMaximum( val )

  def inc(self, val:int=0):
    self.set( self.inner.value() + val )
    self.inner.repaint()

  def setText(self, text:str = ''):
    if text:
      self.inner.setTextVisible(True)
      self.inner.setAlignment(Qt.AlignCenter)
      self.inner.setFormat(text)
    else:
      self.inner.setTextVisible(False)

    self.inner.repaint()

class WelcomeLabel(object):

  def __init__(self, root):
    super().__init__()

    self.inner = root.findChild( QLabel, "welcome_label" )

  def get(self) -> QLabel:
    return self.inner

  def panic(self, text:str='', color:str='#333333'):
    msg = 'Пожалуйста, дождитесь завершения тестирования' if not text else text

    self.inner.setStyleSheet( "QLabel#welcome_label{{ color: {}; }}".format( color ) )
    self.inner.setText( msg )

class TestsTable(object):

  def __init__(self, root):
    super().__init__()

    self.__root             = root
    self.inner :QTableView  = self.__root.findChild( QTableView, "tests_table" )
    self.__data             = []
    self.__model            = TableModel( self.__root, self.__data, ['Статус', 'Описание теста'] )

    self.inner.setSortingEnabled( False )

    # prevent user from selecting rows
    #self.inner.setAttribute(Qt.WA_TransparentForMouseEvents)
    # prevents row selection, but leaves scrolling
    self.inner.mousePressEvent = lambda e: e.ignore()
    self.inner.mouseMoveEvent = lambda e: e.ignore()

    self.inner.setItemDelegateForColumn( 0, CheckBoxDelegate( self.inner ) )


  def get(self) -> QTableView:
    return self.inner

  def add(self, state:int, desc:str ):

    index = QModelIndex()
    self.__model.beginInsertRows( index, len(self.__data), len(self.__data) )
    self.__data.append( [ state, desc ] )
    self.__model.endInsertRows()

  def update(self, row:int = -1, state:int = -2, desc:str = '' ):
    if row < 0:
      self.inner.setModel( self.__model )
      self.inner.setWordWrap(True)
      self.inner.setTextElideMode( Qt.ElideLeft )
      self.inner.horizontalHeader().setStretchLastSection(True)
      self.inner.resizeRowsToContents()
      #self.inner.resizeColumnToContents(1)

    else:
      new_state = self.__data[row][0]
      new_desc  = self.__data[row][1]

      if state > -2:
        new_state = state
      if len( desc ) > 0:
        new_desc = desc

      self.__model.beginResetModel()
      self.__data[row] = [ new_state, new_desc ]
      self.__model.endResetModel()

  def highlight(self, row:int, hex_color:str):
    if row > -1:
      self.__model.beginResetModel()
      self.__model.highlighted_rows[row] = hex_color
      self.__model.endResetModel()

  def select(self, row:int = -1):
    if row < 0:
      self.inner.selectionModel().clear()
    self.inner.selectRow( row )

class ExitButton(object):
  def __init__(self, root):
    super().__init__()

    self.inner :QPushButton = root.findChild( QPushButton, "exit_button" )

    self.inner.clicked.connect( root.close )

  def block(self, val:bool):
    self.inner.setEnabled( not val )

  def setText(self, val:str):
    self.inner.setText( val )

class TableModel(QAbstractTableModel):

  def __init__(self, parent, data, headers, *args):
    QAbstractTableModel.__init__(self, parent, *args)

    self.__data           = data
    self.__headers        = headers
    self.highlighted_rows = dict() # {int:str} - hex color}

  def data(self, index: QModelIndex, role: int):

    if not index.isValid():
      return None

    row = index.row()
    col = index.column()

    # if role == Qt.FontRole and row in self.warning_rows:
    #   font = QFont()
    #   font.setBold(True)
    #   return font

    if role == Qt.TextColorRole and row in self.highlighted_rows.keys():
      return QColor(self.highlighted_rows[row])

    if role == Qt.DisplayRole:
      return self.__data[row][col]

    if role == Qt.CheckStateRole and col == 0:
      return self.__data[row][col]

    elif role == Qt.TextAlignmentRole and col == 0:
      return Qt.AlignCenter

    return None

  def headerData(self, section: int, orientation: Qt.Orientation, role: int):

    if orientation == Qt.Horizontal:

      if section < len( self.__headers ):

        if role == Qt.DisplayRole:
          return self.__headers[section]

        if role == Qt.TextAlignmentRole and section == 1:
          return Qt.AlignHCenter

      else:
        return None

    return None

  def rowCount(self, index):
    return len( self.__data )

  def columnCount(self, index):
    return 2

  def sort(self, column: int, order: Qt.SortOrder):

    #self.emit( SIGNAL( 'layoutAboutToBeChanged()' ) ) #pyside2
    self.layoutAboutToBeChanged.emit()
    self.__data = sorted( self.__data, key=operator.itemgetter( column ) )

    if order == Qt.DescendingOrder:
      self.__data.reverse()

    #self.emit( SIGNAL( 'layoutChanged()' ) ) #pyside2
    self.layoutChanged.emit()

class CheckBoxDelegate(QStyledItemDelegate):

  def paint(self, painter: QPainter, option: QStyleOptionViewItem, index:QModelIndex):

    button      = QStyleOptionButton()
    rect        = QApplication.style().subElementRect( QStyle.SE_CheckBoxIndicator, button )
    button.rect = option.rect

    button.rect.setLeft( option.rect.x() + option.rect.width() // 2 - rect.width() // 2 )
    data = index.data()

    if   data == 1:
      button.state = QStyle.State_On       | QStyle.State_Enabled

    elif data == 0:
      button.state = QStyle.State_Off      | QStyle.State_Enabled

    else:
      button.state = QStyle.State_NoChange | QStyle.State_Enabled

    QApplication.style().drawControl( QStyle.CE_CheckBox, button, painter )


class UIAction:
  """ example: UIAction( action='progress_bar|setValue', args=(15,) ) """

  def __init__(self, **kwargs):
    self.action = kwargs.get( 'action', None )
    self.args   = kwargs.get( 'args' )

class UISignals(QObject):
  action   = pyqtSignal( UIAction )
  error    = pyqtSignal( tuple )
  result   = pyqtSignal( object )

  # msg ( str to stdout, str to stderr )
  msg      = pyqtSignal( str, str )

class UIBridge(QRunnable): # worker, external thread

  class StreamToSignal(object):

    def __init__(self, signals, err:bool=False):
      self.signal = signals
      self.err    = err

    def write(self, buff):
      if self.err:
        self.signals.msg.emit( '', buff )
      else:
        self.signals.msg.emit( buff, '' )

    def flush(self):
      pass

  def __init__(self, *args, **kwargs):
    super().__init__()

    self.target = kwargs.pop( 'target' )
    self.args   = args
    self.kwargs = kwargs

    self.signals = UISignals()

    #sys.stdout = self.StreamToSignal( self.signals, False)
    #sys.stderr = self.StreamToSignal( self.signals, True )

  def action(self, action:str, *args ):
    self.signals.action.emit( UIAction(action=action, args=args) )

  @pyqtSlot()
  def run(self):
    result = None
    try:
      result = self.target( self, *self.args, **self.kwargs )
    except:
      traceback.print_exc()
      exctype, value = sys.exc_info()[:2]
      self.signals.error.emit( (exctype, value, traceback.format_exc()) )
    finally:
      self.signals.result.emit( result )

class UIResponse(QObject):

  def __init__(self):
    super().__init__()

    self.main_window   = MainWindow()
    self.progress_bar  = ProgressBar ( self.main_window.get() )
    self.welcome_label = WelcomeLabel( self.main_window.get() )
    self.tests_table   = TestsTable  ( self.main_window.get() )
    self.exit_button   = ExitButton  ( self.main_window.get() )

    self.threadpool = QThreadPool()
    self.results    = None

  def initWork(self, *args, **kwargs):
    """
        If you need to run any function in another thread,
        this method will make it possible to access the interface elements

        example initWork( arg1, arg1, target=FUNC ),
        FUNC must have UI.UIBridge as its first argument
    """

    worker = UIBridge( *args, **kwargs )
    worker.signals.result.connect( self.complete )
    worker.signals.error.connect( self.error )
    worker.signals.action.connect( self.action )
    worker.signals.msg.connect( self.output )

    self.threadpool.start( worker )

  def result(self):
    if self.threadpool.activeThreadCount() > 0:
      while self.results is None:
        QCoreApplication.processEvents()
      return self.results
    else:
      return self.results

  @pyqtSlot( UIAction )
  def action( self, X:UIAction ):
    target, action = X.action.strip().split('|')
    target = getattr(self, target)
    action = getattr(target, action)
    action( *X.args )

  @pyqtSlot( tuple )
  def error(self, trace:tuple):
    print( trace[2] )

  @pyqtSlot( object )
  def complete(self, result):
    self.results = result

  @pyqtSlot( str, str )
  def output(self, stdout:str, stderr:str):
    if stdout != '':
      print( stdout )
    if stderr != '':
      print( stderr, file=sys.stderr )

def start(ARGS):

  QCoreApplication.setAttribute( Qt.AA_ShareOpenGLContexts )
  app = QApplication( sys.argv )
  app.setStyle(ARGS.style)

  kiosk = BlackWindow()
  kiosk.show()

  # get interface elements as struct UIResponse
  UI = UIResponse()

  if ARGS.ultimate:
    #print('nana', file=sys.stderr)
    UI.main_window.show( frameless=True, noclose=True )
    UI.exit_button.block( True )

  tests = find_all_tests()

  if not tests:
    log.error('Ни одного теста не обнаружено!')
    return 1

  # adding tests to GUI
  for row,test in enumerate(tests):
    test.row = row
    UI.tests_table.add( 0, test.name )

  UI.tests_table.update()
  UI.progress_bar.set( 0 )

  res = 0
  UI.initWork( tests, ARGS, target=run )
  app.exec_()
  res = UI.result()

  return res

# external thread
def run( UI:UIBridge, tests:List[Test], ARGS ):

  failed    = 0
  completed = 0
  errcode   = 0

  UI.action( 'exit_button|block', True )

  # calculate common progress
  UI.action( 'progress_bar|setText', 'вычисление объема работы: {}' )
  UI.action( 'progress_bar|setMax', len(tests))

  common_progress = 0
  for test in tests:
    duration = test.duration()
    test.progress_max = 1 if not duration else int(duration)
    common_progress += test.progress_max
    UI.action( 'progress_bar|setText', f'вычисление объема проверок: {common_progress}' )
    UI.action( 'progress_bar|inc', 1 )

  time.sleep(3)
  UI.action( 'progress_bar|setText' )
  UI.action( 'progress_bar|setMax', common_progress )

  for test in tests:
    #UI.action( 'tests_table|update', test.row, -1 )
    UI.action( 'tests_table|select', test.row )

    if test.start(UI) > 0:
      failed += 1

      UI.action( 'welcome_label|panic', 'Обратитесь к администратору!', '#d92323' )
      UI.action( 'tests_table|highlight', test.row, '#d92323')

      if ARGS.faultcode:
        errcode = test.row + 1
        UI.action( 'progress_bar|fill' )
        break

    else:
      print( f'Тест "{test.name}" успешно пройден.' )
      completed += 1

      UI.action( 'tests_table|update', test.row, 1 )
    UI.action( 'progress_bar|inc', test.estimate() or 1 )

  UI.action( 'tests_table|select' )
  UI.action( 'progress_bar|fill' )
  msg = f'Тестирование завершено. Пройдено: {completed}, Провалено: {failed} .'

  if len( tests ) == completed:
    msg = 'Тестирование завершено успешно.'
    tray_msg( 'Результаты тестирования', msg, 'dialog-information', 3 )

    UI.action( 'exit_button|block', False )
    UI.action( 'main_window|exit' )

    return 0

  else:
    print( msg )

    if ARGS.ultimate:
      UI.action( 'exit_button|setText', 'Завершение сеанса' )
      UI.action( 'main_window|setLogoutExit', True )
      UI.action( 'main_window|setExit', True )

    UI.action( 'exit_button|block', False )

    tray_msg( 'Результаты тестирования', msg, 'dialog-warning', 3 )

    if not ARGS.faultcode:
      errcode = failed

  return errcode


if __name__ == "__main__":
  os.system( 'clear' )
