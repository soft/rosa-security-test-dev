#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import psutil
import pwd

class UnidentifiedProcess(Test):

  name = 'Проверка идентификации'

  def go(self, **kwargs):

    users = dict()
    print('Проверка на дубликаты /etc/passwd...')
    for u in pwd.getpwall():
      print(f'Обнаружен пользователь: {u.pw_name} - {u.pw_uid}')

      if u.pw_name in users.keys():
        raise error(f'Обнаружен дубликат записи для пользователя {u.pw_name} в файле passwd', 1)
      elif u.pw_uid in users.values():
        raise error(f'Обнаружен дубликат записи для пользователя {u.pw_uid} в файле passwd', 2)
      else:
        users[u.pw_name] = u.pw_uid

    print('Проверка правильности ассоциации процессов с пользователями...')
    for proc in psutil.process_iter():
      try:
        username = proc.username()
        uid = proc.uids().real
        print(f'Обнаружен процесс [{proc.pid}]: {username}:{uid}.')

        if username not in users.keys():
          raise error(f'Обнаружен неидентифицированный процесс [{proc.pid}]: user:{username} cmd:{proc.cmdline()}!', 3)
        elif users[proc.username()] != proc.uids().real:
          raise error(f'Обнаружен процесс c неверным UID [{proc.pid}]: uid:{uid} cmd:{proc.cmdline()}!', 4)

      except (Exception):
        continue

    return 0

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    pass