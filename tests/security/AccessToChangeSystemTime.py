#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

from sys import stderr
from kplpack.utils import x

import time

class AccessToChangeSystemTime(Test):

  name = "Проверка ограничений пользователя на изменение системного времени"

  def go(self, **kwargs):
    # if userConGet().u != 'user_u':
    #   print( f'Тест запущен с неправильным контекстом "{userConGet().u}", должен быть "user_u"! ', file=stderr )

    try:
      clock_id   = time.CLOCK_REALTIME
      clock_time = time.clock_gettime( clock_id )

      time.clock_settime( clock_id, clock_time )

      if x('date -s "19 FEB 1993 8:00:00"'): raise PermissionError

    except (PermissionError, FileNotFoundError) as e:
      print( 'Получен ожидаемый отказ в доступе.')
      return 0

    print( 'Ожидаемого отказа при изменении времени не произошло!', file=stderr )
    return 1

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    pass