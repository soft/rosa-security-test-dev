#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import os
from sys import stderr
from kplpack.utils import Context
from kplpack.utils import User
from kplpack.utils import getcon
from kplpack.utils import get_file_mode
from kplpack.utils import colored as I
from kplpack.utils import Walker
from kplpack.utils import QuotedPath as Path

class DeniedFileDirAccess(Test):

  name = 'Проверка ограничений пользователя на доступ к файлам и директориям'
  exclusive = False

  def readable(self, file:Path, comment=''):
    if comment:
      print(f'{comment}: {file.format(underline=True)} ...')
    else:
      print(f'Попытка чтения файла: {file.format(underline=True)}...')

    try:
      with file.open( 'rb' ) as f:
        f.read()
        return True
    except (PermissionError, FileNotFoundError) as e:
      return False

  def writeable(self, file:Path, comment=''):
    if comment:
      print(f'{comment}: "{file}" ...')
    else:
      print(f'Попытка записи в файл: {file.format(underline=True)}...')

    try:
      with file.open( 'wb' ):
        return True
    except (PermissionError, FileNotFoundError) as e:
      return False


  def go(self, **kwargs):

    u_con = User.selinux_getcon()
    dirs = [ Path.home().as_posix(), '/tmp', '/var/tmp', '/run/user' ]

    print(f'Текущий пользователь: {u_con.str}')

    for entry in Walker(dirs):

      mode = get_file_mode( entry )

      if mode == '000':
        print(f'Найден неопознанный файл: {entry.format(underline=True)} { I( mode, 16, 244 ) } : '
              f'{entry.format(underline=True)}\n Пропускаем... ')
        continue
      elif entry.is_symlink():
        print(f'Найдена ссылка: {entry.format(underline=True)} { I( mode, 16, 244 ) } : '
              f'{entry.resolve().format(underline=True)}\n Пропускаем... ')
        continue
      elif entry.is_socket():
        print(f'Найден сокет: {entry.format(underline=True)} { I( mode, 16, 244 ) } \n Пропускаем... ')
        continue
      else:
        print(f'Найден файл: {entry.format(underline=True)} { I( mode, 16, 244 ) } ', end='')

      if mode == '777':
        log.warn(f'{ I( "c правами 777", 124 ) }: {entry.format(underline=True)}!')

      try:

        f_con  = getcon( entry.as_posix() )
        print( f'с контекстом: { I( f_con.str, 16, 244 ) }')

      except (PermissionError, FileNotFoundError) as e:
        print( f'с нечитаемой меткой: {entry.format(underline=True)}.' )
        f_con = Context( s=[-1], c=[-1] )

        err = 0

        if self.readable(entry):
          print('Нарушение безопасности: было произведено чтение файла c нечитаемой меткой! '
                f'{entry.format(underline=True)}', file=stderr)
          err += 1
        else:
          print('Получен ожидаемый отказ в доступе при чтении файла с нечитаемой меткой.')

        if self.writeable(entry):
          print('Нарушение безопасности: была произведена запись в файл с нечитаемой меткой!'
                f'{entry.format(underline=True)}', file=stderr)
          err += 1
        else:
          print('Получен ожидаемый отказ в доступе при записи в файл с нечитаемой меткой.')

        if err:
          return 9 + err
        else:
          continue

      try:
        if u_con.str and f_con.str:

          if len(f_con.s) > 1 and not entry.is_dir():
            raise error('Нарушение безопасности: обнаружен файл с диапазоном уровней!'
                        f'{entry.format(underline=True)}', 2)

          if u_con.s > f_con.s:

            if self.writeable(entry):
              raise error(f'Нарушение безопасности: была произведена запись в файл низшего уровня!'
                          f' {entry.format(underline=True)}', 3)
            else:
              print('Получен ожидаемый отказ в доступе при записи в файл низшего уровня.')

          if u_con.s < f_con.s:

            if self.readable(entry):
              raise error(f'Нарушение безопасности: было произведено чтение файла высшего уровня! \
                           {entry.format(underline=True)}', 4)
            else:
              print( 'Получен ожидаемый отказ в доступе при чтении файла высшего уровня.')

          if u_con.s == f_con.s:
            if not u_con.c and f_con.c:

              if self.readable(entry, 'Попытка чтение файла с категорией, от пользователя без категории'):
                raise error(f'Нарушение безопасности: было произведено чтение файла с категорией\
                              от пользователя без категории! {entry.format(underline=True)}', 5)
              else:
                print('Получен ожидаемый отказ в доступе при чтении файла с категорией от пользователя без категории.')

            if u_con.c and f_con.c and not (set(u_con.c) >= set(f_con.c)):

              if self.readable(entry, 'Попытка чтения файла с другой категорией'):
                raise error(f'Нарушение безопасности: было произведено чтение файла с другой категорией! \
                              {entry.format(underline=True)}', 6)
              else:
                print('Получен ожидаемый отказ в доступе при чтении файла с другой категорией.')

              if self.writeable(entry, 'Попытка записи в файл с другой категорией'):
                raise error(f'Нарушение безопасности: была произведена запись в файл с другой категорией! \
                             {entry.format(underline=True)}', 7)
              else:
                print('Получен ожидаемый отказ в доступе при записи в файл с другой категорией.')

      except (PermissionError, FileNotFoundError, OSError) as e:
        if e.__class__.__name__ == 'OSError' and e.errno != 6:
          return 8

        print( 'Получен ожидаемый отказ в доступе.')
        continue

    print( 'Проверка на запись в корневые директории...' )
    dirs = [ '/' + d  for d in os.listdir('/') ]
    dirs.append('/')
    dirs.remove('/tmp')

    for d in dirs:
      try:
        if not os.path.isdir(d): continue

        print( f'Обнаружена директория: "{I(d, underline=True)}", попытка записи...' )
        with open( f'{d}/777.txt', 'a+' ) as f:
          f.write( 'Этот файл не должен существовать.' )
          raise error(f'Нарушение безопасности: была произведена запись в корневой каталог "{I(d, underline=True)}"!', 9)

      except (PermissionError, FileNotFoundError) as e:
        print( 'Получен ожидаемый отказ в доступе.')
        continue

    # print( 'Проверка возможности чтения /var/log...')

    # for dir_path, _, file_name in os.walk( '/var/log' ):
    #   for f in file_name:
    #     entry = f'{dir_path}/{f}'

    #     try:
    #       with open( entry, 'r' ) as f:
    #         f.readlines()
    #         print( 'Нарушение безопасности: было произведено чтение файла из директории /var/log!', file=stderr )
    #         return 10

    #     except (PermissionError, FileNotFoundError) as e:
    #       print( 'Получен ожидаемый отказ в доступе.')
    #       continue

    return 0

  def clean(self, **kwargs):

    dirs = [ '/' + d  for d in os.listdir('/') ]
    dirs.append('/')
    dirs.remove('/tmp')

    for d in dirs:
      if os.path.exists( f'{d}/777.txt' ):
        os.remove( f'{d}/777.txt' )

  def duration(self, **kwargs):
    pass