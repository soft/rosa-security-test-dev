#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import logging
from kplpack.utils import SystemCtl
from kplpack.utils import StreamToLog
from kplpack.utils import colored as I


class AuditServices(Test):

  name = 'Проверка сервисов аудита'

  def go(self, **kwargs):
    log = logging.getLogger("rst")

    services = [
      'systemd-journald.service',
      'rsyslog.service',
      'auditd.service',
      'rosa-central-panel-serverd.service'
    ]

    achtung_words = [
      'wrong',
      'error',
      'invalid',
      'fault',
      'terminated',
      'killed',
      'suspended'
    ]

    sysctl = SystemCtl(StreamToLog(log))

    print('Проверка сервисов аудита:')
    for s in services:
      status = sysctl.status(s)

      if not status:
        raise error(f'Для сервиса {s} не был получен статус!', 1)
      else:

        if status.active != 'active':
          journal_part = " \n".join(status.journal) or "нет записей."
          raise error(f'Сервис {s} отключен({status.load})! Выписка из журнала: {journal_part}', 2)
        else:
          print(f'Сервис {s} запущен.')
          journal_part = status.journal

          found = False
          for i,line in enumerate(journal_part):
            for word in achtung_words:
              if word in line:
                found = True
                journal_part[i] = line.replace(word, I(word, 9))

          if found:
            journal_part = " \n".join(journal_part)
            log.warning(f'В выписке из журнала для сервиса {s} обнаружены записи требующие внимания: \n{journal_part}')

    return 0

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    pass