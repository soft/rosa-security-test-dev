#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import psutil
import os
import grp
import pathlib
import select

from kplpack.utils import Ticker, colored as I
from kplpack.utils import Walker
from kplpack.utils import get_file_mode


class WriteNotRead(Test):
  name = 'Утилита: тест более низкоуровневой функции прохода'
  exclusive = True

  # + /bin
  # + /dev
  #
  # settings
  target_dir = '/lost+found'
  exclude_groups = ['wheel']
  exclude_objects = ('/dev/urandom', '/dev/random', '/dev/zero')

  def available(self, file:pathlib.Path, mode='r' or 'w' or 'e', timeout:float=0.0):
    if mode == 'r':
      readable, _, _ = select.select([file], [], [], timeout)
      if file in readable:
        return True
    elif mode == 'w':
      _, writable, _ = select.select([], [file], [], timeout)
      if file in writable:
        return True
    elif mode == 'e':
      _, _, executable = select.select([], [], [file], timeout)
      if file in executable:
        return True

    return False

  def readable(self, file:pathlib.Path):
    try:
      with file.open( 'rb' ) as f:
        if self.available(f, 'r', 5):
          os.read(f.fileno(), 1)
          return True
        return None
    except (PermissionError, FileNotFoundError) as e:
      return False

  def writable(self, file:pathlib.Path, comment=''):
    print(2)
    try:
      with file.open( 'wb' ) as f:
        if self.available(f, 'w', 5):
          os.write(f.fileno(), b'')
          return True
        return None
    except (PermissionError, FileNotFoundError) as e:
      return False

  def cansave(self, file:pathlib.Path):
    print(3)
    old_content = None
    try:
      with file.open('rb') as backup_file:
        old_content = backup_file.read()
    except:
      return False

    try:
      with file.open('ab') as file_to_write:
        file_to_write.write('🎈'.encode())

      with file.open('rb') as file_to_rw:
        data = file_to_rw.read()
        if data.find('🎈'.encode()) >= 0:
          return True

    except OSError as e:
      return False

    finally:
      with file.open('wb') as restore_file:
        restore_file.write(old_content)

  def fastwalk(self, path:str = ''):
    if path == '':
      path = '/'

    for entry in os.scandir(path):
      try:
        if entry.is_dir(follow_symlinks=False):
          yield from self.fastwalk(entry.path)
      except PermissionError:
        continue
      yield entry

  def go(self, **kwargs):
    os.sync()

    i = 0
    with Ticker(precision=3) as timer:
      for i,l in enumerate(Walker(('/'))):
        pass #print(f'[{get_file_mode(l)}] : {l.path}' )
    print(timer.last_point, i)

    i = 0
    with Ticker(precision=3) as timer:
      for i,l in enumerate(Walker(('/'))):
        print(get_file_mode(l))
    print(timer.last_point, i)

    i = 0
    with Ticker(precision=3) as timer:

      for i,l in enumerate(self.fastwalk()):
        pass #print(f'[{get_file_mode(l)}] : {l.path}' )
    print(timer.last_point, i)

    return 0


  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    path, files_count = next(Walker([self.target_dir]).count())
    return files_count