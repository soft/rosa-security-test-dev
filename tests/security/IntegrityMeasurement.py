#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

import logging
import pathlib
import os

from kplpack.utils import check_kernel_config

class IntegrityMeasurement(Test):

  name = 'Проверка механизма подписей'
  exclusive = False

  def go(self, **kwargs):
    log = logging.getLogger("rst")
    kernel_parameters = {
      'CONFIG_IMA': 'y',
      'CONFIG_EVM': 'y'
    }

    print('Проверка конфигурации ядра...')

    for kp,val in kernel_parameters.items():
      if check_kernel_config(kp, val) == 0:
        print(f'Параметр {kp} обнаружен.')
      else:
        raise error(f'Параметр {kp} не обнаружен или имеет неверное значение!', 0)

    print('Проверка поддержки IMA/EVM...')
    imafs = pathlib.Path('/sys/kernel/security/integrity/ima/')

    if imafs.exists():
      print('Механизм проверки подписей включен.')
    else:
      raise error(f'Механизм проверки подписей отключен.', 1)

    print('Проверка подписи исполняемых файлов...')
    runnable = pathlib.Path('~/.runnable').expanduser()
    runnable.touch(0o777)

    if os.system(runnable.as_posix()) == 0:
      raise error('Был запущен неподписанный файл.', 2)

    return 0

  def clean(self, **kwargs):
    runnable = pathlib.Path('~/.runnable').expanduser()
    runnable.unlink(missing_ok=True)

  def duration(self, **kwargs):
    pass