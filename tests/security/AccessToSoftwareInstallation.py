#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

from sys import stderr
from kplpack.utils import x

class AccessToSoftwareInstallation(Test):

  name = "Проверка ограничений пользователя на установку программного обеспечения"

  def go(self, **kwargs):

    # if userConGet().u != 'user_u':
    #   print( f'Тест запущен с неправильным контекстом "{userConGet().u}", должен быть "user_u"! ', file=stderr )

    try:
      if x('dnf install dos2unix'): raise PermissionError
      if x('dnf remove dos2unix') : raise PermissionError

    except (PermissionError, FileNotFoundError) as e:
      print( 'Получен ожидаемый отказ в доступе.')
      return 0

    log.error( 'Ожидаемого отказа в доступе к установке\удалению ПО не произошло!')
    return 1

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    pass