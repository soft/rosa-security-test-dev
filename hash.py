# -*- coding: utf-8 -*-
# kernelplv@gmail.com

import os
import sys
import argparse
import platform
from getpass import getpass
from pathlib import Path
from subprocess import run
from sys import stderr, stdin, stdout
from tkinter.tix import WINDOW
from typing import Tuple, Union

OS = platform.system().lower()
WINDOWS = OS == 'windows'
LINUX = OS == 'linux'
MACOS = OS == 'darwin'

self = Path(__file__)

def x( command:str, direct=False ) -> Union[int, Tuple[int, str]]:
  if direct:
    if WINDOWS:
      process = run( ['powershell', command], shell=True, stdout=stdout, stderr=stderr, stdin=stdin )
    else:
      process = run( command, shell=True, encoding='UTF-8', stdout=stdout, stderr=stderr, stdin=stdin )
    return process.returncode
  else:
    if WINDOWS:
      process = run( ['powershell', command], shell=True, capture_output=True )
    else:
      process = run( command, shell=True, encoding='UTF-8', capture_output=True )
  return (process.returncode, process.stdout)

def pName() -> str:

  git_config = self.parent.joinpath( Path('.git/config') )

  if git_config.exists():

    with git_config.open('r') as conf:
      for line in conf.readlines():
        if 'url' in line:
          return line.split('=')[1].strip().split('/')[-1].split('.')[0]

    print( 'hash.py: project name not found' )
    exit(1)

  else:
    print( 'hash.py: .git/config not found' )
    exit(1)

def pTag() -> str:

  if LINUX:
    code, project_tag = x( 'git tag -l | tail -1' )
  elif WINDOWS:
    code, project_tag = x( 'git tag -l | Select-Object -last 1' )
  else:
    print('Unsupported OS!')
    exit(1)

  if code != 0:
    print( 'hash.py: tags not found' )
    exit(1)

  return project_tag.strip()

def uName() -> str:

  code, name = x( 'git config user.name' )

  if code != 0:
    print( 'hash.py: tags not found' )
    exit(1)

  return name.strip()

if __name__ == "__main__":

  try:
    import requests
  except ModuleNotFoundError:
    print('Please install requests module. Example: python -m pip install requests, dnf install python38-requests.')

  parser  = argparse.ArgumentParser( )
  parser.description = 'This tool should be located in the root of the git project,\
                        it allows you to get sha1 from the project archive located \
                        at file-store.rosalinux.ru'

  parser.add_argument( 'tag', nargs='?', type=str, default='',
                       help='git tag, if not specified, sha1 will be found by the last project tag' )

  cmdline = parser.parse_args( sys.argv[1:] )

  os.chdir( self.parent )

  user, proj, tag = uName(), pName(), cmdline.tag if cmdline.tag else pTag()

  hash_request  = f'https://abf.io/{user}/{proj}/get_sha1_of_archive/{proj}-{tag}.tar.gz'
  login_request = 'https://abf.io/users/sign_in'

  with requests.Session() as ss:

    response           = ss.get(login_request)
    source             = response.content.decode('UTF-8').split('name="csrf-token" content="')[1].split('"')[0]
    authenticity_token = source

    form = { 'authenticity_token'  : authenticity_token,
             'user[login]'         : 'kernelplv',
             'user[password]'      : getpass( 'Password [abf.io]: ' ),
             'user[remember_me]'   : 0,
             'commit'              : 'Sign in' }

    response = ss.post(login_request, data=form)
    response = ss.get(hash_request)
    response.raise_for_status()

    print( '{} : {}'.format( tag, response.content.decode('UTF-8') ) )
