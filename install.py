#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

# known issues
# if the shortcut is placed in a different path, uninstall does not work for it

from rst import __version__ as ver

import os
import selinux
import argparse

#from PIL import Image
from pathlib import Path
from subprocess import run
from kplpack.utils import check_depends
from shutil import rmtree, copytree, copyfile, ignore_patterns

CURRENT_DIR   = os.path.dirname(os.path.abspath(__file__))
LABEL_DIR = '/usr/share/applications'
LINK_DIR  = '/usr/bin'
ICONS_DIR = '/usr/share/icons/hicolor'
CONTEXT   = 'user_u:object_r:usr_t:s0'

# Create uninstall
INSTALLED_DIR       = 'none'
INSTALLED_LABEL     = 'none'
INSTALLED_ICONS_DIR = 'none'
INSTALLED_LINK      = 'none'


def extract_pth(path:str) -> str:
  if 'build' in path.lower():
    return ''.join( [ '/' + pth for pth in path.split('/')[5:] ] )
  else:
    return path

def update_string(install_dir:str, install_var:str, new_val:str, custom_file:str=''):
  """ Case sensitive
  """

  F = install_dir + '/install.py' if custom_file == '' else custom_file

  new_val = extract_pth( new_val )

  with open( F, 'r' ) as f:
    content = f.readlines()
    for i, l in enumerate(content):
      if install_var in l:
        content[i] = l.replace( 'none', new_val )
        break

  with open( F, 'r+' ) as f:
    f.writelines( content )

def install_icons(setup_path:str = ICONS_DIR, uninstall=False):
  """ Warning: hardcoded to %{buildroot}%{_datadir}/icons/hicolor
  """

  from PIL import Image

  ico512 = f'{CURRENT_DIR}/gui/icon512.png'

  sizes  = [ (16,16),
             (22,22),
             (24,24),
             (32,32),
             (36,36),
             (48,48),
             (64,64),
             (72,72),
             (96,96),
             (128,128),
             (192,192),
             (256,256),
             (512,512)
           ]

  for size in sizes:
    p = Path( f'{setup_path}/{size[0]}x{size[1]}/apps/rosa-security-test.png' )

    if uninstall:
      if p.parent.exists():
        rmtree( p.parent.as_posix() )
    else:
      p.parent.mkdir( parents=True, exist_ok=True )
      img = Image.open( ico512 )
      img = img.resize( size ).save( p.as_posix() )
      p.chmod( 0o644 )

def setcon(path:str='/usr/share/rosa-security-test', context:str=CONTEXT):
  for dir_path, dirs, files in os.walk( path ):
    for f in files:
      f_path = f'{dir_path}/{f}'
      selinux.setfilecon_raw( f_path, context )
    for d in dirs:
      d_path = f'{dir_path}/{d}'
      selinux.setfilecon_raw( d_path, context )

if __name__ == '__main__':

  parser             = argparse.ArgumentParser( add_help=False, usage=argparse.SUPPRESS )
  parser.description =  f'\nУстановщик rosa-security-test'
  parser.epilog      = 'kernelplv@gmail.com // www.rosalinux.ru 2020©'

  group1 = parser.add_argument_group('Общее')
  group1.add_argument( '-h', '--help', action='help', default=argparse.SUPPRESS,
                       help='Показать это сообщение и завершить работу')
  group1.add_argument( dest='action', type=str, choices=['install','uninstall'], help='Установка\удаление' )

  group1.add_argument( '-d', '--install-dir', dest='path', default='/usr/share', action='store',
                       help='По-умолчанию установка производится в /usr/share/. Этот параметр\
                             позволяет указать другой путь установки.')
  group1.add_argument( '-s', '--shortcut-dir', dest='path_shortcut', default=LABEL_DIR, action='store',
                       help= f'По-умолчанию ярлык помещается в {LABEL_DIR}. Этот параметр\
                              позволяет указать другой путь размещения.')
  group1.add_argument( '-i', '--icons-dir', dest='path_icons', default=ICONS_DIR, action='store',
                       help= f'По-умолчанию иконки помещаются в {ICONS_DIR}. Этот параметр\
                              позволяет указать другой путь размещения.')
  group1.add_argument( '-l', '--link-dir', dest='path_link', default=LINK_DIR, action='store',
                       help= f'По-умолчанию ссылка помещается в {LINK_DIR}. Этот параметр\
                              позволяет указать другой путь размещения.')
  group1.add_argument( '--context', dest='se_context', default=CONTEXT, action='store',
                      help= f'По-умолчанию файл программы имеют метку {CONTEXT}. Этот параметр\
                            позволяет указать другую метку.')


  args = parser.parse_args()

  os.system('clear')

  if args.action == "install":

    if not check_depends( 'PIL', 'python38-pillow' ):
      exit(1)

    try:
      # install tree
      install_dir = Path( args.path + '/rosa-security-test')

      if install_dir.exists():
        print( f'rosa-security-test уже установлено в {install_dir}. Переустановка.')
        out = run( f'{install_dir}/install.py uninstall', shell=True, capture_output=True ).stdout
        print( out.decode('UTF-8') )

      install_dir.mkdir( parents=True, exist_ok=True )
      install_dir = install_dir.as_posix()

      copytree( CURRENT_DIR, install_dir, dirs_exist_ok=True, ignore=ignore_patterns('.vscode', '.git', '.gitignore', '*.desktop') )
      print( f'rosa-security-test установлено в {install_dir}')

      setcon( install_dir, args.se_context )
      selinux.setfilecon_raw( install_dir, args.se_context )

      print( f'контекстные метки selinux проставлены {args.se_context}.' )

      update_string( install_dir, 'INSTALLED_DIR', install_dir )
      update_string( '', 'INSTALLED_DIR', install_dir, install_dir + '/rst.py' )

      # install link
      whereis = f'{args.path_link}/rst'

      os.symlink( '/usr/share/rosa-security-test/rst.py', whereis ) # it doesn't matter when building the package
      update_string( install_dir, 'INSTALLED_LINK', whereis )


      if args.path_shortcut != 'none':

        # install shortcut
        if os.path.exists( args.path_shortcut ):
          p = Path( f'{args.path_shortcut}/rosa-security-test.desktop' )
        else:
          print( f'Заданного каталога не существует: {args.path_shortcut}.' )
          exit(1)

        copyfile( f'{CURRENT_DIR}/gui/rosa-security-test.desktop', p.as_posix() )
        p.chmod( 0o755 )

        print( f'ярлык приложения установлен в /usr/share/applications' )
        update_string( install_dir, 'INSTALLED_LABEL', p.as_posix() )

        # change path to launcher in shortcut
        if install_dir != '/usr/share/rosa-security-test':
          print( f'замена путей в ярлыке на {install_dir}' )
          content = None
          with open( f'{args.path_shortcut}/rosa-security-test.desktop', 'r' ) as f:
            content = f.read()
            content = content.replace( f'/usr/share/rosa-security-test/rst.py',
                                       f'{ extract_pth(install_dir) }/rst.py' )
          with open( f'{args.path_shortcut}/rosa-security-test.desktop', 'r+' ) as f:
            f.write( content )

        # install icons
        p = Path( args.path_icons )

        install_icons( p.as_posix() )
        print( f'иконки установлены в {ICONS_DIR}' )
        update_string( install_dir, 'INSTALLED_ICONS_DIR', p.as_posix() )

    except Exception as e:
      print( f'Ошибка установки: {e}')

  if args.action == "uninstall":
    try:

      if INSTALLED_DIR != 'none':
        rmtree( INSTALLED_DIR )
        print( f'rosa-security-test удалено из {INSTALLED_DIR}')

      if INSTALLED_LABEL != 'none':
        os.remove( f'{INSTALLED_LABEL}' )
        print( f'ярлык удален из {INSTALLED_LABEL}')

      if INSTALLED_ICONS_DIR != 'none':
        install_icons( INSTALLED_ICONS_DIR, uninstall=True )
        print( f'иконки удалены из {INSTALLED_ICONS_DIR}')

      if INSTALLED_LINK != 'none':
        os.unlink( f'{INSTALLED_LINK}' )
        print( f'ссылка удалена из {INSTALLED_LINK}')

    except Exception as e:
      print( f'Ошибка удаления: {e}')

# install.py install -s /usr/share/applications
# install.py install -d /home/fafa/ -s /home/fafa/shortcut -i /home/fafa/icons -l /home/fafa