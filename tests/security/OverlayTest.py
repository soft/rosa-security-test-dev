#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

# - required for every test --------------
from TestClass import TestError as error
from TestClass import Test
import logging
log = logging.getLogger("rst")
# ----------------------------------------

from pathlib import Path
from kplpack.utils import User
from kplpack.utils import Return, x


class OverlayTest(Test):

  name = 'Проверка виртуализации служебных файлов'
  exclusive = False

  def go(self, **kwargs):

    u_con = User.selinux_getcon()

    if u_con.s[-1] <= 0:
      print(f'Проверка оверлея не требуется на текущем уровне конфиденциальности: {u_con.s}.')
      return 0

    res = x( 'df -a --output=fstype,target | grep overlay', Return.process )

    if res.returncode != 0:
      raise error(f'Не удалось получить список смонтированных каталогов оверлея: {res.stdout.read()}', 1)

    check_list = [ '.config', '.cache', '.local' ]

    for l in res.stdout.readlines():
      p = Path(l.split(' ')[-1].strip('\n'))

      if p.name in check_list:
        check_list.remove(p.name)

    if len(check_list) > 0:
      raise error(f'Не смонтирован(ы) следующие каталоги оверлея: {check_list} .', 2)

    return 0

  def clean(self, **kwargs):
    pass

  def duration(self, **kwargs):
    pass