#!/usr/bin/python3.8
# -*- coding: utf-8 -*-

#_NAME=Проверка загруженных модулей ядра
#_EXCLUSIVE=

import logging
import pathlib
import re

from kplpack.utils import TestError as error
from kplpack.utils import colored as I

#The first column contains the name of the module.
#The second column refers to the memory size of the module, in bytes.
#The third column lists how many instances of the module are currently loaded. A value of zero represents an unloaded module.
#The fourth column states if the module depends upon another module to be present in order to function, and lists those other modules.
#The fifth column lists what load state the module is in: Live, Loading, or Unloading are the only possible values.
#The sixth column lists the current kernel memory offset for the loaded module. This information can be useful for debugging purposes, or for profiling tools such as oprofile.

def run():
  log = logging.getLogger("rst")

  modinfo = pathlib.Path('/proc/modules')
  reg = r'(?:(?P<name>\w*?) (?P<size>\d*) (?P<instances>\d*) (?P<depends>.*) Live (?P<offset>.*))'

  required_modules = {
    'azaz': False
  }

  if modinfo.exists():
    with modinfo.open('r') as f:
      data = f.readlines()
      data.sort()
      for line in data:
        name, size, instances, depends, offset = re.findall(reg, line, re.IGNORECASE)[0]
        depends = depends.split(',')[:-1]

        print(f'Обнаружен модуль {I(name, 73):38} Кол-во экземпляров: {instances}.')

        if name in required_modules.keys():
          if int(instances) > 0:
            required_modules[name] = True

  for mod,loaded in required_modules.items():
    if not loaded:
      raise error(f'Модуль {mod} не загружен!', 1)

  return 0

def Clean(**kwargs):
  pass

def Go(**kwargs):
  return run()